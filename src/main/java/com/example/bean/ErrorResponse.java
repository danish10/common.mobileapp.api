package com.example.bean;

import org.springframework.http.HttpStatus;

public class ErrorResponse {
	
	 private HttpStatus status;
	  private String id;
	  private String message;
	  
	  public ErrorResponse() {}
	  
	  public ErrorResponse(HttpStatus status, String id, String message)
	  {
	    this.status = status;
	    this.id = id;
	    this.message = message;
	  }
	  
	  public HttpStatus getStatus()
	  {
	    return this.status;
	  }
	  
	  public void setStatus(HttpStatus status)
	  {
	    this.status = status;
	  }
	  
	  public String getMessage()
	  {
	    return this.message;
	  }
	  
	  public void setMessage(String message)
	  {
	    this.message = message;
	  }
	  
	  public String getId()
	  {
	    return this.id;
	  }
	  
	  public void setId(String id)
	  {
	    this.id = id;
	  }
	
}
