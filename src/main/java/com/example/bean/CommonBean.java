package com.example.bean;

public class CommonBean
{
  private String colstr1;
  private String colstr2;
  private String colstr3;
  private String colstr4;
  private String colstr5;
  private String colstr6;
  private String colstr7;
  private String colstr8;
  private String colstr9;
  private String colstr10;
  private String colstr11;
  private String colstr12;
  private String colstr13;
  private String colstr14;
  private String colstr15;
  private int colint1;
  private int colint2;
  private int colint3;
  private int colint4;
  private int colint5;
  private int colint6;
  private int colint7;
  private int colint8;
  private int colint9;
  private int colint10;
  private boolean colbool1;
  private boolean colbool2;
  private boolean colbool3;
  private boolean colbool4;
  private boolean colbool5;
  
  public String getColstr1()
  {
    return this.colstr1;
  }
  
  public void setColstr1(String colstr1)
  {
    this.colstr1 = colstr1;
  }
  
  public String getColstr2()
  {
    return this.colstr2;
  }
  
  public void setColstr2(String colstr2)
  {
    this.colstr2 = colstr2;
  }
  
  public String getColstr3()
  {
    return this.colstr3;
  }
  
  public void setColstr3(String colstr3)
  {
    this.colstr3 = colstr3;
  }
  
  public String getColstr4()
  {
    return this.colstr4;
  }
  
  public void setColstr4(String colstr4)
  {
    this.colstr4 = colstr4;
  }
  
  public String getColstr5()
  {
    return this.colstr5;
  }
  
  public void setColstr5(String colstr5)
  {
    this.colstr5 = colstr5;
  }
  
  public String getColstr6()
  {
    return this.colstr6;
  }
  
  public void setColstr6(String colstr6)
  {
    this.colstr6 = colstr6;
  }
  
  public String getColstr7()
  {
    return this.colstr7;
  }
  
  public void setColstr7(String colstr7)
  {
    this.colstr7 = colstr7;
  }
  
  public String getColstr8()
  {
    return this.colstr8;
  }
  
  public void setColstr8(String colstr8)
  {
    this.colstr8 = colstr8;
  }
  
  public String getColstr9()
  {
    return this.colstr9;
  }
  
  public void setColstr9(String colstr9)
  {
    this.colstr9 = colstr9;
  }
  
  public String getColstr10()
  {
    return this.colstr10;
  }
  
  public void setColstr10(String colstr10)
  {
    this.colstr10 = colstr10;
  }
  
  public String getColstr11()
  {
    return this.colstr11;
  }
  
  public void setColstr11(String colstr11)
  {
    this.colstr11 = colstr11;
  }
  
  public String getColstr12()
  {
    return this.colstr12;
  }
  
  public void setColstr12(String colstr12)
  {
    this.colstr12 = colstr12;
  }
  
  public String getColstr13()
  {
    return this.colstr13;
  }
  
  public void setColstr13(String colstr13)
  {
    this.colstr13 = colstr13;
  }
  
  public String getColstr14()
  {
    return this.colstr14;
  }
  
  public void setColstr14(String colstr14)
  {
    this.colstr14 = colstr14;
  }
  
  public String getColstr15()
  {
    return this.colstr15;
  }
  
  public void setColstr15(String colstr15)
  {
    this.colstr15 = colstr15;
  }
  
  public int getColint1()
  {
    return this.colint1;
  }
  
  public void setColint1(int colint1)
  {
    this.colint1 = colint1;
  }
  
  public int getColint2()
  {
    return this.colint2;
  }
  
  public void setColint2(int colint2)
  {
    this.colint2 = colint2;
  }
  
  public int getColint3()
  {
    return this.colint3;
  }
  
  public void setColint3(int colint3)
  {
    this.colint3 = colint3;
  }
  
  public int getColint4()
  {
    return this.colint4;
  }
  
  public void setColint4(int colint4)
  {
    this.colint4 = colint4;
  }
  
  public int getColint5()
  {
    return this.colint5;
  }
  
  public void setColint5(int colint5)
  {
    this.colint5 = colint5;
  }
  
  public int getColint6()
  {
    return this.colint6;
  }
  
  public void setColint6(int colint6)
  {
    this.colint6 = colint6;
  }
  
  public int getColint7()
  {
    return this.colint7;
  }
  
  public void setColint7(int colint7)
  {
    this.colint7 = colint7;
  }
  
  public int getColint8()
  {
    return this.colint8;
  }
  
  public void setColint8(int colint8)
  {
    this.colint8 = colint8;
  }
  
  public int getColint9()
  {
    return this.colint9;
  }
  
  public void setColint9(int colint9)
  {
    this.colint9 = colint9;
  }
  
  public int getColint10()
  {
    return this.colint10;
  }
  
  public void setColint10(int colint10)
  {
    this.colint10 = colint10;
  }
  
  public boolean isColbool1()
  {
    return this.colbool1;
  }
  
  public void setColbool1(boolean colbool1)
  {
    this.colbool1 = colbool1;
  }
  
  public boolean isColbool2()
  {
    return this.colbool2;
  }
  
  public void setColbool2(boolean colbool2)
  {
    this.colbool2 = colbool2;
  }
  
  public boolean isColbool3()
  {
    return this.colbool3;
  }
  
  public void setColbool3(boolean colbool3)
  {
    this.colbool3 = colbool3;
  }
  
  public boolean isColbool4()
  {
    return this.colbool4;
  }
  
  public void setColbool4(boolean colbool4)
  {
    this.colbool4 = colbool4;
  }
  
  public boolean isColbool5()
  {
    return this.colbool5;
  }
  
  public void setColbool5(boolean colbool5)
  {
    this.colbool5 = colbool5;
  }
}
