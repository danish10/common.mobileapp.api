package com.example.bean;

public class Test
{
  private long id;
  private String text1;
  private String text2;
  
  public long getId()
  {
    return this.id;
  }
  
  public void setId(long id)
  {
    this.id = id;
  }
  
  public String getText1()
  {
    return this.text1;
  }
  
  public void setText1(String text1)
  {
    this.text1 = text1;
  }
  
  public String getText2()
  {
    return this.text2;
  }
  
  public void setText2(String text2)
  {
    this.text2 = text2;
  }
}
