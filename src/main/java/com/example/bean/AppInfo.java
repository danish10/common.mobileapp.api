package com.example.bean;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.NumberSerializer;
import com.fasterxml.jackson.databind.ser.std.StringSerializer;
import io.swagger.annotations.ApiModel;

@ApiModel("Provides information about the current build of the app.")
public class AppInfo
{
  private String releaseNumber;
  private String buildNumber;
  private String timeStamp;
  
  @JsonSerialize(using=StringSerializer.class)
  public String getReleaseNumber()
  {
    return this.releaseNumber;
  }
  
  public void setReleaseNumber(String releaseNumber)
  {
    this.releaseNumber = releaseNumber;
  }
  
  @JsonSerialize(using=NumberSerializer.class)
  public String getBuildNumber()
  {
    return this.buildNumber;
  }
  
  public void setBuildNumber(String buildNumber)
  {
    this.buildNumber = buildNumber;
  }
  
  @JsonSerialize(using=StringSerializer.class)
  public String getTimeStamp()
  {
    return this.timeStamp;
  }
  
  public void setTimeStamp(String timeStamp)
  {
    this.timeStamp = timeStamp;
  }
  
  public String toString()
  {
    return "AppInfo [releaseNumber=" + this.releaseNumber + ", buildNumber=" + this.buildNumber + ", timeStamp=" + this.timeStamp + "]";
  }
}
