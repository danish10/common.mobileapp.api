package com.example.bean;

import java.io.Serializable;
import java.util.List;

public class ApiBean
  implements Serializable
{
  private static final long serialVersionUID = -753424559402938769L;
  private int id;
  private String gsno;
  private String tpin;
  private String name;
  private String email;
  private String mobile;
  private String aadhar;
  private String userid;
  private String dob;
  private String dao;
  private String password;
  private String dor;
  private String permdate;
  private String sex;
  private int married;
  private String dom;
  private String relation_name;
  private String pto_no;
  private long sus_no;
  private String confirmation_date;
  private String do2_date;
  private String course_desc;
  private String do2_type;
  private String status;
  private String reason_for;
  private String punish_desc;
  private String punish_date;
  private String offense;
  private String promotion_desc;
  private String prom_date;
  private String seniority_date;
  private String probation_date;
  private String prob_clear_flag;
  private String posting_desc;
  private String tos_date;
  private String sos_date;
  private String medal_date;
  private String medal_desc;
  private String macp_desc;
  private String gre_name;
  private String macp_date;
  private String leave_from;
  private String leave_to;
  private int nodays;
  private String remark;
  private String leave_desc;
  private String house_no;
  private String village;
  private String pin_code;
  private String post;
  private String tehsil;
  private String dist_name;
  private String state_name;
  private String country_name;
  private String trade_desc;
  private String unit_desc;
  private String sector_desc;
  private String area_desc;
  private String subarea_desc;
  private String project_desc;
  private String nrs;
  private String nrs_km;
  private String imei;
  private String text1;
  private String text2;
  private String award_desc;
  private String award_date;
  private String course_code;
  private String course_from;
  private String course_to;
  private String institute;
  private String ins_address;
  private String grade_desc;
  private String education_code;
  private String education_desc;
  private String year_pass;
  private String gpf_type;
  private String gpf_year;
  private String amount;
  private String sanction_date;
  private String complaint_datetime;
  private String grievancedesc;
  private String grievancegp_desc;
  private String med_cat_code;
  private String medcat_desc;
  private String med_cat_type;
  private String msl_seniority;
  private String from_date;
  private String to_date;
  private String maritial_status;
  private String joint_photo;
  private String jointPhoto_url;
  private String wife_photo;
  private String wifePhoto_url;
  private String image_url;
  private String casualty;
  private String group;
  private String university;
  private List<String> document;
  private List<String> coursedocument;
  private String salaryUrl;
  private String form16Url;
  private String imageBase64;
  private String grievance_type;
  private String grievance_message;
  private String grievance_reply;
  private String grievancedatetime;
  //course
  private String courseResult;
  private String upcomingCourse;
  //posting
  private String from_unit;
  private String to_unit;
  private String posting_order_no;
  private String movedate;
  
  public String getGrievance_type()
  {
    return this.grievance_type;
  }
  
  public void setGrievance_type(String grievance_type)
  {
    this.grievance_type = grievance_type;
  }
  
  public String getGrievance_message()
  {
    return this.grievance_message;
  }
  
  public void setGrievance_message(String grievance_message)
  {
    this.grievance_message = grievance_message;
  }
  
  public String getGrievance_reply()
  {
    return this.grievance_reply;
  }
  
  public void setGrievance_reply(String grievance_reply)
  {
    this.grievance_reply = grievance_reply;
  }
  
  public String getGrievancedatetime()
  {
    return this.grievancedatetime;
  }
  
  public void setGrievancedatetime(String grievancedatetime)
  {
    this.grievancedatetime = grievancedatetime;
  }
  
  public String getGroup()
  {
    return this.group;
  }
  
  public void setGroup(String group)
  {
    this.group = group;
  }
  
  public String getAward_desc()
  {
    return this.award_desc;
  }
  
  public void setAward_desc(String award_desc)
  {
    this.award_desc = award_desc;
  }
  
  public String getAward_date()
  {
    return this.award_date;
  }
  
  public void setAward_date(String award_date)
  {
    this.award_date = award_date;
  }
  
  public String getCourse_code()
  {
    return this.course_code;
  }
  
  public void setCourse_code(String course_code)
  {
    this.course_code = course_code;
  }
  
  public String getCourse_from()
  {
    return this.course_from;
  }
  
  public void setCourse_from(String course_from)
  {
    this.course_from = course_from;
  }
  
  public String getCourse_to()
  {
    return this.course_to;
  }
  
  public void setCourse_to(String course_to)
  {
    this.course_to = course_to;
  }
  
  public String getInstitute()
  {
    return this.institute;
  }
  
  public void setInstitute(String institute)
  {
    this.institute = institute;
  }
  
  public String getIns_address()
  {
    return this.ins_address;
  }
  
  public void setIns_address(String ins_address)
  {
    this.ins_address = ins_address;
  }
  
  public String getGrade_desc()
  {
    return this.grade_desc;
  }
  
  public void setGrade_desc(String grade_desc)
  {
    this.grade_desc = grade_desc;
  }
  
  public String getEducation_code()
  {
    return this.education_code;
  }
  
  public void setEducation_code(String education_code)
  {
    this.education_code = education_code;
  }
  
  public String getEducation_desc()
  {
    return this.education_desc;
  }
  
  public void setEducation_desc(String education_desc)
  {
    this.education_desc = education_desc;
  }
  
  public String getYear_pass()
  {
    return this.year_pass;
  }
  
  public void setYear_pass(String year_pass)
  {
    this.year_pass = year_pass;
  }
  
  public String getGpf_type()
  {
    return this.gpf_type;
  }
  
  public void setGpf_type(String gpf_type)
  {
    this.gpf_type = gpf_type;
  }
  
  public String getGpf_year()
  {
    return this.gpf_year;
  }
  
  public void setGpf_year(String gpf_year)
  {
    this.gpf_year = gpf_year;
  }
  
  public String getAmount()
  {
    return this.amount;
  }
  
  public void setAmount(String amount)
  {
    this.amount = amount;
  }
  
  public String getSanction_date()
  {
    return this.sanction_date;
  }
  
  public void setSanction_date(String sanction_date)
  {
    this.sanction_date = sanction_date;
  }
  
  public String getComplaint_datetime()
  {
    return this.complaint_datetime;
  }
  
  public void setComplaint_datetime(String complaint_datetime)
  {
    this.complaint_datetime = complaint_datetime;
  }
  
  public String getGrievancedesc()
  {
    return this.grievancedesc;
  }
  
  public void setGrievancedesc(String grievancedesc)
  {
    this.grievancedesc = grievancedesc;
  }
  
  public String getGrievancegp_desc()
  {
    return this.grievancegp_desc;
  }
  
  public void setGrievancegp_desc(String grievancegp_desc)
  {
    this.grievancegp_desc = grievancegp_desc;
  }
  
  public String getMed_cat_code()
  {
    return this.med_cat_code;
  }
  
  public void setMed_cat_code(String med_cat_code)
  {
    this.med_cat_code = med_cat_code;
  }
  
  public String getMedcat_desc()
  {
    return this.medcat_desc;
  }
  
  public void setMedcat_desc(String medcat_desc)
  {
    this.medcat_desc = medcat_desc;
  }
  
  public String getMed_cat_type()
  {
    return this.med_cat_type;
  }
  
  public void setMed_cat_type(String med_cat_type)
  {
    this.med_cat_type = med_cat_type;
  }
  
  public String getMsl_seniority()
  {
    return this.msl_seniority;
  }
  
  public void setMsl_seniority(String msl_seniority)
  {
    this.msl_seniority = msl_seniority;
  }
  
  public String getFrom_date()
  {
    return this.from_date;
  }
  
  public void setFrom_date(String from_date)
  {
    this.from_date = from_date;
  }
  
  public String getTo_date()
  {
    return this.to_date;
  }
  
  public void setTo_date(String to_date)
  {
    this.to_date = to_date;
  }
  
  public String getMaritial_status()
  {
    return this.maritial_status;
  }
  
  public void setMaritial_status(String maritial_status)
  {
    this.maritial_status = maritial_status;
  }
  
  public String getTpin()
  {
    return this.tpin;
  }
  
  public void setTpin(String tpin)
  {
    this.tpin = tpin;
  }
  
  public String getImage_url()
  {
    return this.image_url;
  }
  
  public void setImage_url(String image_url)
  {
    this.image_url = image_url;
  }
  
  public int getId()
  {
    return this.id;
  }
  
  public void setId(int id)
  {
    this.id = id;
  }
  
  public String getGsno()
  {
    return this.gsno;
  }
  
  public void setGsno(String gsno)
  {
    this.gsno = gsno;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public void setName(String name)
  {
    this.name = name;
  }
  
  public String getDob()
  {
    return this.dob;
  }
  
  public void setDob(String dob)
  {
    this.dob = dob;
  }
  
  public String getDao()
  {
    return this.dao;
  }
  
  public void setDao(String dao)
  {
    this.dao = dao;
  }
  
  public String getDor()
  {
    return this.dor;
  }
  
  public void setDor(String dor)
  {
    this.dor = dor;
  }
  
  public String getPermdate()
  {
    return this.permdate;
  }
  
  public void setPermdate(String permdate)
  {
    this.permdate = permdate;
  }
  
  public String getSex()
  {
    return this.sex;
  }
  
  public void setSex(String sex)
  {
    this.sex = sex;
  }
  
  public int getMarried()
  {
    return this.married;
  }
  
  public void setMarried(int married)
  {
    this.married = married;
  }
  
  public String getDom()
  {
    return this.dom;
  }
  
  public void setDom(String dom)
  {
    this.dom = dom;
  }
  
  public String getRelation_name()
  {
    return this.relation_name;
  }
  
  public void setRelation_name(String relation_name)
  {
    this.relation_name = relation_name;
  }
  
  
  public String getPto_no() {
	return pto_no;
}

public void setPto_no(String pto_no) {
	this.pto_no = pto_no;
}

public long getSus_no()
  {
    return this.sus_no;
  }
  
  public void setSus_no(long sus_no)
  {
    this.sus_no = sus_no;
  }
  
  public String getConfirmation_date()
  {
    return this.confirmation_date;
  }
  
  public void setConfirmation_date(String confirmation_date)
  {
    this.confirmation_date = confirmation_date;
  }
  
  public String getDo2_date()
  {
    return this.do2_date;
  }
  
  public void setDo2_date(String do2_date)
  {
    this.do2_date = do2_date;
  }
  
  public String getCourse_desc()
  {
    return this.course_desc;
  }
  
  public void setCourse_desc(String course_desc)
  {
    this.course_desc = course_desc;
  }
  
  public String getDo2_type()
  {
    return this.do2_type;
  }
  
  public void setDo2_type(String do2_type)
  {
    this.do2_type = do2_type;
  }
  
  public String getStatus()
  {
    return this.status;
  }
  
  public void setStatus(String status)
  {
    this.status = status;
  }
  
  public String getReason_for()
  {
    return this.reason_for;
  }
  
  public void setReason_for(String reason_for)
  {
    this.reason_for = reason_for;
  }
  
  public String getPunish_desc()
  {
    return this.punish_desc;
  }
  
  public void setPunish_desc(String punish_desc)
  {
    this.punish_desc = punish_desc;
  }
  
  public String getPunish_date()
  {
    return this.punish_date;
  }
  
  public void setPunish_date(String punish_date)
  {
    this.punish_date = punish_date;
  }
  
  public String getOffense()
  {
    return this.offense;
  }
  
  public void setOffense(String offense)
  {
    this.offense = offense;
  }
  
  public String getPromotion_desc()
  {
    return this.promotion_desc;
  }
  
  public void setPromotion_desc(String promotion_desc)
  {
    this.promotion_desc = promotion_desc;
  }
  
  public String getProm_date()
  {
    return this.prom_date;
  }
  
  public void setProm_date(String prom_date)
  {
    this.prom_date = prom_date;
  }
  
  public String getSeniority_date()
  {
    return this.seniority_date;
  }
  
  public void setSeniority_date(String seniority_date)
  {
    this.seniority_date = seniority_date;
  }
  
  public String getProbation_date()
  {
    return this.probation_date;
  }
  
  public void setProbation_date(String probation_date)
  {
    this.probation_date = probation_date;
  }
  

public String getProb_clear_flag() {
	return prob_clear_flag;
}

public void setProb_clear_flag(String prob_clear_flag) {
	this.prob_clear_flag = prob_clear_flag;
}

public String getPosting_desc()
  {
    return this.posting_desc;
  }
  
  public void setPosting_desc(String posting_desc)
  {
    this.posting_desc = posting_desc;
  }
  
  public String getTos_date()
  {
    return this.tos_date;
  }
  
  public void setTos_date(String tos_date)
  {
    this.tos_date = tos_date;
  }
  
  public String getSos_date()
  {
    return this.sos_date;
  }
  
  public void setSos_date(String sos_date)
  {
    this.sos_date = sos_date;
  }
  
  public String getMedal_date()
  {
    return this.medal_date;
  }
  
  public void setMedal_date(String medal_date)
  {
    this.medal_date = medal_date;
  }
  
  public String getMedal_desc()
  {
    return this.medal_desc;
  }
  
  public void setMedal_desc(String medal_desc)
  {
    this.medal_desc = medal_desc;
  }
  
  public String getMacp_desc()
  {
    return this.macp_desc;
  }
  
  public void setMacp_desc(String macp_desc)
  {
    this.macp_desc = macp_desc;
  }
  
  public String getGre_name()
  {
    return this.gre_name;
  }
  
  public void setGre_name(String gre_name)
  {
    this.gre_name = gre_name;
  }
  
  public String getMacp_date()
  {
    return this.macp_date;
  }
  
  public void setMacp_date(String macp_date)
  {
    this.macp_date = macp_date;
  }
  
  public String getLeave_from()
  {
    return this.leave_from;
  }
  
  public void setLeave_from(String leave_from)
  {
    this.leave_from = leave_from;
  }
  
  public String getLeave_to()
  {
    return this.leave_to;
  }
  
  public void setLeave_to(String leave_to)
  {
    this.leave_to = leave_to;
  }
  
  public int getNodays()
  {
    return this.nodays;
  }
  
  public void setNodays(int nodays)
  {
    this.nodays = nodays;
  }
  
  public String getRemark()
  {
    return this.remark;
  }
  
  public void setRemark(String remark)
  {
    this.remark = remark;
  }
  
  public String getLeave_desc()
  {
    return this.leave_desc;
  }
  
  public void setLeave_desc(String leave_desc)
  {
    this.leave_desc = leave_desc;
  }
  
  public String getHouse_no()
  {
    return this.house_no;
  }
  
  public void setHouse_no(String house_no)
  {
    this.house_no = house_no;
  }
  
  public String getVillage()
  {
    return this.village;
  }
  
  public void setVillage(String village)
  {
    this.village = village;
  }
  
  public String getPost()
  {
    return this.post;
  }
  
  public void setPost(String post)
  {
    this.post = post;
  }
  
  public String getTehsil()
  {
    return this.tehsil;
  }
  
  public void setTehsil(String tehsil)
  {
    this.tehsil = tehsil;
  }
  
  public String getDist_name()
  {
    return this.dist_name;
  }
  
  public void setDist_name(String dist_name)
  {
    this.dist_name = dist_name;
  }
  
  public String getState_name()
  {
    return this.state_name;
  }
  
  public void setState_name(String state_name)
  {
    this.state_name = state_name;
  }
  
  public String getCountry_name()
  {
    return this.country_name;
  }
  
  public void setCountry_name(String country_name)
  {
    this.country_name = country_name;
  }
  
  public String getTrade_desc()
  {
    return this.trade_desc;
  }
  
  public void setTrade_desc(String trade_desc)
  {
    this.trade_desc = trade_desc;
  }
  
  public String getUnit_desc()
  {
    return this.unit_desc;
  }
  
  public void setUnit_desc(String unit_desc)
  {
    this.unit_desc = unit_desc;
  }
  
  public String getSector_desc()
  {
    return this.sector_desc;
  }
  
  public void setSector_desc(String sector_desc)
  {
    this.sector_desc = sector_desc;
  }
  
  public String getArea_desc()
  {
    return this.area_desc;
  }
  
  public void setArea_desc(String area_desc)
  {
    this.area_desc = area_desc;
  }
  
  public String getSubarea_desc()
  {
    return this.subarea_desc;
  }
  
  public void setSubarea_desc(String subarea_desc)
  {
    this.subarea_desc = subarea_desc;
  }
  
  public String getProject_desc()
  {
    return this.project_desc;
  }
  
  public void setProject_desc(String project_desc)
  {
    this.project_desc = project_desc;
  }
  
  public String getNrs()
  {
    return this.nrs;
  }
  
  public void setNrs(String nrs)
  {
    this.nrs = nrs;
  }
  
  public String getText1()
  {
    return this.text1;
  }
  
  public void setText1(String text1)
  {
    this.text1 = text1;
  }
  
  public String getText2()
  {
    return this.text2;
  }
  
  public void setText2(String text2)
  {
    this.text2 = text2;
  }
  
  public String getCasualty()
  {
    return this.casualty;
  }
  
  public void setCasualty(String casualty)
  {
    this.casualty = casualty;
  }
  
  public String getPin_code()
  {
    return this.pin_code;
  }
  
  public void setPin_code(String pin_code)
  {
    this.pin_code = pin_code;
  }
  
  public String getUniversity()
  {
    return this.university;
  }
  
  public void setUniversity(String university)
  {
    this.university = university;
  }
  
  public String getPassword()
  {
    return this.password;
  }
  
  public void setPassword(String password)
  {
    this.password = password;
  }
  
  public List<String> getDocument()
  {
    return this.document;
  }
  
  public void setDocument(List<String> document)
  {
    this.document = document;
  }
  
  public String getSalaryUrl()
  {
    return this.salaryUrl;
  }
  
  public void setSalaryUrl(String salaryUrl)
  {
    this.salaryUrl = salaryUrl;
  }
  
  public String getForm16Url()
  {
    return this.form16Url;
  }
  
  public void setForm16Url(String form16Url)
  {
    this.form16Url = form16Url;
  }
  
  public String getImageBase64()
  {
    return this.imageBase64;
  }
  
  public void setImageBase64(String imageBase64)
  {
    this.imageBase64 = imageBase64;
  }
  
  public String getEmail()
  {
    return this.email;
  }
  
  public void setEmail(String email)
  {
    this.email = email;
  }
  
  public String getMobile()
  {
    return this.mobile;
  }
  
  public void setMobile(String mobile)
  {
    this.mobile = mobile;
  }
  
  public String getAadhar()
  {
    return this.aadhar;
  }
  
  public void setAadhar(String aadhar)
  {
    this.aadhar = aadhar;
  }
  
  public String getUserid()
  {
    return this.userid;
  }
  
  public void setUserid(String userid)
  {
    this.userid = userid;
  }
  
  
  public String getNrs_km() {
	return nrs_km;
}

public void setNrs_km(String nrs_km) {
	this.nrs_km = nrs_km;
}

public String getImei() {
	return imei;
}

public void setImei(String imei) {
	this.imei = imei;
}

public String getJoint_photo()
  {
    return this.joint_photo;
  }
  
  public void setJoint_photo(String joint_photo)
  {
    this.joint_photo = joint_photo;
  }
  
  public String getJointPhoto_url()
  {
    return this.jointPhoto_url;
  }
  
  public void setJointPhoto_url(String jointPhoto_url)
  {
    this.jointPhoto_url = jointPhoto_url;
  }
  
  public String getWife_photo()
  {
    return this.wife_photo;
  }
  
  public void setWife_photo(String wife_photo)
  {
    this.wife_photo = wife_photo;
  }
  
  public String getWifePhoto_url()
  {
    return this.wifePhoto_url;
  }
  
  public void setWifePhoto_url(String wifePhoto_url)
  {
    this.wifePhoto_url = wifePhoto_url;
  }

public List<String> getCoursedocument() {
	return coursedocument;
}

public void setCoursedocument(List<String> coursedocument) {
	this.coursedocument = coursedocument;
}

public String getFrom_unit() {
	return from_unit;
}

public void setFrom_unit(String from_unit) {
	this.from_unit = from_unit;
}

public String getTo_unit() {
	return to_unit;
}

public void setTo_unit(String to_unit) {
	this.to_unit = to_unit;
}

public String getPosting_order_no() {
	return posting_order_no;
}

public void setPosting_order_no(String posting_order_no) {
	this.posting_order_no = posting_order_no;
}

public String getMovedate() {
	return movedate;
}

public void setMovedate(String movedate) {
	this.movedate = movedate;
}

public String getCourseResult() {
	return courseResult;
}

public void setCourseResult(String courseResult) {
	this.courseResult = courseResult;
}

public String getUpcomingCourse() {
	return upcomingCourse;
}

public void setUpcomingCourse(String upcomingCourse) {
	this.upcomingCourse = upcomingCourse;
}

@Override
public String toString() {
	return "ApiBean [id=" + id + ", gsno=" + gsno + ", tpin=" + tpin + ", name=" + name + ", email=" + email
			+ ", mobile=" + mobile + ", aadhar=" + aadhar + ", userid=" + userid + ", dob=" + dob + ", dao=" + dao
			+ ", password=" + password + ", dor=" + dor + ", permdate=" + permdate + ", sex=" + sex + ", married="
			+ married + ", dom=" + dom + ", relation_name=" + relation_name + ", pto_no=" + pto_no + ", sus_no="
			+ sus_no + ", confirmation_date=" + confirmation_date + ", do2_date=" + do2_date + ", course_desc="
			+ course_desc + ", do2_type=" + do2_type + ", status=" + status + ", reason_for=" + reason_for
			+ ", punish_desc=" + punish_desc + ", punish_date=" + punish_date + ", offense=" + offense
			+ ", promotion_desc=" + promotion_desc + ", prom_date=" + prom_date + ", seniority_date=" + seniority_date
			+ ", probation_date=" + probation_date + ", prob_clear_flag=" + prob_clear_flag + ", posting_desc="
			+ posting_desc + ", tos_date=" + tos_date + ", sos_date=" + sos_date + ", medal_date=" + medal_date
			+ ", medal_desc=" + medal_desc + ", macp_desc=" + macp_desc + ", gre_name=" + gre_name + ", macp_date="
			+ macp_date + ", leave_from=" + leave_from + ", leave_to=" + leave_to + ", nodays=" + nodays + ", remark="
			+ remark + ", leave_desc=" + leave_desc + ", house_no=" + house_no + ", village=" + village + ", pin_code="
			+ pin_code + ", post=" + post + ", tehsil=" + tehsil + ", dist_name=" + dist_name + ", state_name="
			+ state_name + ", country_name=" + country_name + ", trade_desc=" + trade_desc + ", unit_desc=" + unit_desc
			+ ", sector_desc=" + sector_desc + ", area_desc=" + area_desc + ", subarea_desc=" + subarea_desc
			+ ", project_desc=" + project_desc + ", nrs=" + nrs + ", nrs_km=" + nrs_km + ", imei=" + imei + ", text1="
			+ text1 + ", text2=" + text2 + ", award_desc=" + award_desc + ", award_date=" + award_date
			+ ", course_code=" + course_code + ", course_from=" + course_from + ", course_to=" + course_to
			+ ", institute=" + institute + ", ins_address=" + ins_address + ", grade_desc=" + grade_desc
			+ ", education_code=" + education_code + ", education_desc=" + education_desc + ", year_pass=" + year_pass
			+ ", gpf_type=" + gpf_type + ", gpf_year=" + gpf_year + ", amount=" + amount + ", sanction_date="
			+ sanction_date + ", complaint_datetime=" + complaint_datetime + ", grievancedesc=" + grievancedesc
			+ ", grievancegp_desc=" + grievancegp_desc + ", med_cat_code=" + med_cat_code + ", medcat_desc="
			+ medcat_desc + ", med_cat_type=" + med_cat_type + ", msl_seniority=" + msl_seniority + ", from_date="
			+ from_date + ", to_date=" + to_date + ", maritial_status=" + maritial_status + ", joint_photo="
			+ joint_photo + ", jointPhoto_url=" + jointPhoto_url + ", wife_photo=" + wife_photo + ", wifePhoto_url="
			+ wifePhoto_url + ", image_url=" + image_url + ", casualty=" + casualty + ", group=" + group
			+ ", university=" + university + ", document=" + document + ", coursedocument=" + coursedocument
			+ ", salaryUrl=" + salaryUrl + ", form16Url=" + form16Url + ", imageBase64=" + imageBase64
			+ ", grievance_type=" + grievance_type + ", grievance_message=" + grievance_message + ", grievance_reply="
			+ grievance_reply + ", grievancedatetime=" + grievancedatetime + ", courseResult=" + courseResult
			+ ", upcomingCourse=" + upcomingCourse + ", from_unit=" + from_unit + ", to_unit=" + to_unit
			+ ", posting_order_no=" + posting_order_no + ", movedate=" + movedate + "]";
}
  

  
}
