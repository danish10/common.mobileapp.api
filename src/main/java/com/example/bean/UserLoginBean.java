package com.example.bean;

import java.util.Date;

import javax.persistence.Column;

public class UserLoginBean
{
  private String id;
  private String userid;
  private String gsno;
  private Date dob;
  private String aadhar;
  private String email;
  private String mobile;
  private String password;
  private String url;
  private String imei;
  private String imageBase64;
  private String jointBase64;
  private String joint_photo;
  private String jointPhoto_url;
  private String wife_photo;
  private String wifePhoto_url;
  
  public UserLoginBean() {}
  
 
  
  public UserLoginBean(String id, String userid, String gsno, Date dob, String aadhar, String email, String mobile,
		String password, String url, String imei, String imageBase64, String jointBase64, String joint_photo,
		String jointPhoto_url, String wife_photo, String wifePhoto_url) {
	super();
	this.id = id;
	this.userid = userid;
	this.gsno = gsno;
	this.dob = dob;
	this.aadhar = aadhar;
	this.email = email;
	this.mobile = mobile;
	this.password = password;
	this.url = url;
	this.imei = imei;
	this.imageBase64 = imageBase64;
	this.jointBase64 = jointBase64;
	this.joint_photo = joint_photo;
	this.jointPhoto_url = jointPhoto_url;
	this.wife_photo = wife_photo;
	this.wifePhoto_url = wifePhoto_url;
}



public String getId()
  {
    return this.id;
  }
  
  public void setId(String id)
  {
    this.id = id;
  }
  
  public String getUserid()
  {
    return this.userid;
  }
  
  public void setUserid(String userid)
  {
    this.userid = userid;
  }
  
  public String getGsno()
  {
    return this.gsno;
  }
  
  public void setGsno(String gsno)
  {
    this.gsno = gsno;
  }
  
  public Date getDob()
  {
    return this.dob;
  }
  
  public void setDob(Date dob)
  {
    this.dob = dob;
  }
  
  public String getAadhar()
  {
    return this.aadhar;
  }
  
  public void setAadhar(String aadhar)
  {
    this.aadhar = aadhar;
  }
  
  public String getEmail()
  {
    return this.email;
  }
  
  public void setEmail(String email)
  {
    this.email = email;
  }
  
  public String getMobile()
  {
    return this.mobile;
  }
  
  public void setMobile(String mobile)
  {
    this.mobile = mobile;
  }
  
  public String getPassword()
  {
    return this.password;
  }
  
  public void setPassword(String password)
  {
    this.password = password;
  }
  
  public String getUrl()
  {
    return this.url;
  }
  
  public void setUrl(String url)
  {
    this.url = url;
  }
  
  public String getImageBase64()
  {
    return this.imageBase64;
  }
  
  public void setImageBase64(String imageBase64)
  {
    this.imageBase64 = imageBase64;
  }
  
  public String getJoint_photo()
  {
    return this.joint_photo;
  }
  
  public void setJoint_photo(String joint_photo)
  {
    this.joint_photo = joint_photo;
  }
  
  public String getJointPhoto_url()
  {
    return this.jointPhoto_url;
  }
  
  public void setJointPhoto_url(String jointPhoto_url)
  {
    this.jointPhoto_url = jointPhoto_url;
  }
  
  public String getWife_photo()
  {
    return this.wife_photo;
  }
  
  public void setWife_photo(String wife_photo)
  {
    this.wife_photo = wife_photo;
  }
  
  public String getWifePhoto_url()
  {
    return this.wifePhoto_url;
  }
  
  public void setWifePhoto_url(String wifePhoto_url)
  {
    this.wifePhoto_url = wifePhoto_url;
  }
  
  public String getJointBase64()
  {
    return this.jointBase64;
  }
  
  public void setJointBase64(String jointBase64)
  {
    this.jointBase64 = jointBase64;
  }



public String getImei() {
	return imei;
}



public void setImei(String imei) {
	this.imei = imei;
}



@Override
public String toString() {
	return "UserLoginBean [id=" + id + ", userid=" + userid + ", gsno=" + gsno + ", dob=" + dob + ", aadhar=" + aadhar
			+ ", email=" + email + ", mobile=" + mobile + ", password=" + password + ", url=" + url + ", imei=" + imei
			+ ", imageBase64=" + imageBase64 + ", jointBase64=" + jointBase64 + ", joint_photo=" + joint_photo
			+ ", jointPhoto_url=" + jointPhoto_url + ", wife_photo=" + wife_photo + ", wifePhoto_url=" + wifePhoto_url
			+ "]";
}
  
}
