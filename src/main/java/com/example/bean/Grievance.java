package com.example.bean;

public class Grievance
{
  private int id;
  private String gsno;
  private String grievance_type;
  private String grievance_message;
  private String grievance_reply;
  private String grievancedatetime;
  
  public Grievance() {}
  
  public Grievance(int id, String gsno, String grievance_type, String grievance_message, String grievance_reply, String grievancedatetime)
  {
    this.id = id;
    this.gsno = gsno;
    this.grievance_type = grievance_type;
    this.grievance_message = grievance_message;
    this.grievance_reply = grievance_reply;
    this.grievancedatetime = grievancedatetime;
  }
  
  public int getId()
  {
    return this.id;
  }
  
  public void setId(int id)
  {
    this.id = id;
  }
  
  public String getGsno()
  {
    return this.gsno;
  }
  
  public void setGsno(String gsno)
  {
    this.gsno = gsno;
  }
  
  public String getGrievance_type()
  {
    return this.grievance_type;
  }
  
  public void setGrievance_type(String grievance_type)
  {
    this.grievance_type = grievance_type;
  }
  
  public String getGrievance_message()
  {
    return this.grievance_message;
  }
  
  public void setGrievance_message(String grievance_message)
  {
    this.grievance_message = grievance_message;
  }
  
  public String getGrievance_reply()
  {
    return this.grievance_reply;
  }
  
  public void setGrievance_reply(String grievance_reply)
  {
    this.grievance_reply = grievance_reply;
  }
  
  public String getGrievancedatetime()
  {
    return this.grievancedatetime;
  }
  
  public void setGrievancedatetime(String grievancedatetime)
  {
    this.grievancedatetime = grievancedatetime;
  }
}
