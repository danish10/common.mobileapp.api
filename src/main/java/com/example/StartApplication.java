package com.example;

import com.example.bean.AppInfo;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableAutoConfiguration
public class StartApplication
  extends SpringBootServletInitializer
  implements ApplicationContextAware
{
  @Autowired
  private Environment props;
  private static ApplicationContext context;
  
  public static void main(String[] args)
  {
    SpringApplication.run(StartApplication.class, args);
  }
  
  protected SpringApplicationBuilder configure(SpringApplicationBuilder builder)
  {
    return builder.sources(new Class[] { StartApplication.class });
  }
  
  public void setApplicationContext(ApplicationContext ctx)
  {
    setApplicationContextStatic(ctx);
  }
  
  public static void setApplicationContextStatic(ApplicationContext ctx)
  {
    context = ctx;
  }
  
  public static ApplicationContext getApplicationContext()
  {
    return context;
  }
  
  @Bean
  public AppInfo getAppInfo()
  {
    AppInfo appinfo = new AppInfo();
    appinfo.setBuildNumber(this.props.getProperty("buildNumber"));
    appinfo.setReleaseNumber(this.props.getProperty("releaseNumber"));
    appinfo.setTimeStamp(this.props.getProperty("buildTimeStamp"));
    return appinfo;
  }
  
  @Bean
  public Docket swaggerApi()
  {
    String profile = this.props.getProperty("spring.profiles.active");
    

    boolean isUIEnabled = (!"PROD".equalsIgnoreCase(profile)) && (!"PRODUCTION".equalsIgnoreCase(profile)) && (!"PR".equalsIgnoreCase(profile));
    
    return new Docket(DocumentationType.SWAGGER_2).enable(isUIEnabled).select().build().apiInfo(apiInfo());
  }
  
  private ApiInfo apiInfo()
  {
    return new ApiInfo(this.props.getProperty("appName"), this.props
      .getProperty("appDescription"), this.props
      .getProperty("buildNumber"), null, this.props
      
      .getProperty("contactEmail"), null, null);
  }
  
  @Bean
  public RestTemplate loadedRestTemplate()
  {
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
    List<ClientHttpRequestInterceptor> interceptors = new ArrayList();
    
    restTemplate.setInterceptors(interceptors);
    return restTemplate;
  }
}
