package com.example.file.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import com.example.entity.PostingEntity;
import com.example.entity.SalaryFileEntity;
import com.example.service.IGenericService;

@Service("file")
public class UploadFileService {

	private static final Logger logger = LoggerFactory.getLogger(UploadFileService.class);
	@Value("${salary.path}")
	private String SALARY_PATH;

	@Value("${policy.path}")
	private String POLICY_PATH;

	@Value("${form16.path}")
	private String FORM16_PATH;

	@Value("${courseResult.path}")
	private String COURSE_RESULT_PATH;
	
	@Value("${upcomingCourse.path}")
	private String UPCOMING_COURSE_PATH;

	
	@Value("${posting.path}")
	private String POSTING_PATH;

	
	@Autowired
	private IGenericService<PostingEntity> iPostingEntityService;
	
	
	boolean posting_file_upload_status;
	
	public Resource loadFile(String filename) {
		try {
			
			Path rootLocation = Paths.get(FORM16_PATH);
			Path file = rootLocation.resolve(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new RuntimeException("FAIL!");
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("FAIL!");
		}
	}
	
	
	/****************************************************************************************************************************/
	public String uploadPostingDocument(MultipartFile file, String loginId) {

		SalaryFileEntity objfileUpload = new SalaryFileEntity();
		String Posting_File="";

		try {
	
			Posting_File = uploadPostingDeailsDocuments(loginId, file);
			
			// objfileUpload.setUploadTextDocument(MOBILE_DOCUMENT !=null ? MOBILE_DOCUMENT
			// : null);
			// objfileUpload.setLoginId(loginId);
			// iDataGenericService.update(objfileUpload);
			System.out.println(Boolean.toString(posting_file_upload_status));
			
			return Boolean.toString(posting_file_upload_status);

		} catch (Exception e) {
			// TODO: handle exception
			// ReportOfUploadFile(RIGHT_NUMBER_DATA,WROUND_NUMBER_DATA,DUPLICATE_NUMBER_DATA);
			System.out.println("e  " + e);
			logger.info("######## Error in File Upload ########" + e.getMessage());
			return Boolean.toString(posting_file_upload_status);
		}
	}
	
	private String uploadPostingDeailsDocuments(String loginId, MultipartFile multipartFile) {

		String newDocName = "";
		String fileNamePrefix = "";
		String MONTH = "";
		String YEAR = "";
		String NEW_FILE_NAME = "";
		

		try {
			if (!multipartFile.isEmpty()) {
				File doc = new File(POSTING_PATH + "/");
				logger.info("DOUCMENT PATH : " + doc);
				if (!doc.exists()) {
					logger.info("Directory Not Exists : " + doc);
					doc.mkdirs();
				}

				String fileName = "";
				String s = multipartFile.getOriginalFilename();
				String[] words = s.split("\\s+");
				for (int i = 0; i < words.length; i++) {
					fileName = fileName + words[i].replaceAll(" ", "");
					System.out.println("Trim File Name" + fileName);
				}

			

				NEW_FILE_NAME = loginId+ "_" + fileName;
				System.out.println("*********** : " + NEW_FILE_NAME);

				String folder_name = POSTING_PATH + "/" + NEW_FILE_NAME;
				logger.info("Orignal Folder Name : " + folder_name);
				File file = new File(folder_name);
				if (fileNamePrefix != null) {

					newDocName = fileNamePrefix + NEW_FILE_NAME;
					logger.info("New Document Name With FileNamePrefix : " + newDocName);
				} else {
					newDocName = NEW_FILE_NAME;
					logger.info("New Document Name Without FileNamePrefix : " + newDocName);

				}
				String newPath = POSTING_PATH + "/" + newDocName;
				logger.info("Rename File : " + newPath);
				File newFile = new File(newPath);

				if (!file.isDirectory()) {
					file.renameTo(newFile);
				}
				byte[] bytes = multipartFile.getBytes();
				Path path = Paths.get(newPath);
				logger.info("path : " + path);
				Files.write(path, bytes);
				
				posting_file_upload_status = uploadPostingExcelFile(newFile);
				
			}
		} catch (IOException e) {
			logger.info("IOException : " + e.getMessage());
			newDocName = "";
		}
		return newDocName;
	}

	public boolean uploadPostingExcelFile(File excel_file) throws IOException{

		boolean upload_status;
		try {
			
			File fileName = new File(excel_file.getAbsolutePath());
			
			FileInputStream fis = new FileInputStream(fileName);

			XSSFWorkbook book = new XSSFWorkbook(fis);

			XSSFSheet sheet = book.getSheetAt(0);
			DataFormatter dataFormatter = new DataFormatter();

			Iterator rows = sheet.rowIterator();

			while (rows.hasNext()) {
				XSSFRow row = (XSSFRow) rows.next();

				PostingEntity pEntity = new PostingEntity();
				DataFormatter dataFormatter1 = new DataFormatter();

				String no;
				if (row.getCell(0) != null) {

					Cell cellValue0 = row.getCell(0);
					dataFormatter1.formatCellValue(cellValue0);
					no = dataFormatter1.formatCellValue(cellValue0);
				} else {
					no = null;
				}

				String gsno;
				if (row.getCell(1) != null) {
					Cell cellValue1 = row.getCell(1);
					dataFormatter1.formatCellValue(cellValue1);
					gsno = dataFormatter1.formatCellValue(cellValue1);
					// System.out.println(gsno);
				} else {
					gsno = null;
				}

				String from_unit;
				if (row.getCell(2) != null) {
					Cell cellValue2 = row.getCell(2);
					dataFormatter1.formatCellValue(cellValue2);
					from_unit = dataFormatter1.formatCellValue(cellValue2);
					// System.out.println(from_unit);
				} else {
					from_unit = null;
				}

				String to_unit;
				if (row.getCell(3) != null) {
					Cell cellValue3 = row.getCell(3);
					dataFormatter1.formatCellValue(cellValue3);
					to_unit = dataFormatter1.formatCellValue(cellValue3);
					// System.out.println(to_unit);
				} else {
					to_unit = null;
				}

				String posting_order_no;
				if (row.getCell(4) != null) {
					Cell cellValue4 = row.getCell(4);
					dataFormatter1.formatCellValue(cellValue4);
					posting_order_no = dataFormatter1.formatCellValue(cellValue4);
					// System.out.println(posting_order_no);
				} else {
					posting_order_no = null;
				}

				String nrs;
				if (row.getCell(5) != null) {
					Cell cellValue5 = row.getCell(5);
					dataFormatter1.formatCellValue(cellValue5);
					nrs = dataFormatter1.formatCellValue(cellValue5);
					// System.out.println(nrs);
				} else {
					nrs = null;
				}

				String movedate;
				if (row.getCell(6) != null) {
					Cell cellValue6 = row.getCell(6);
					dataFormatter1.formatCellValue(cellValue6);
					movedate = dataFormatter1.formatCellValue(cellValue6);
					// System.out.println(movedate);
				} else {
					movedate = null;
				}
				pEntity.setGsno(gsno);
				pEntity.setFrom_unit(from_unit);
				pEntity.setTo_unit(to_unit);
				pEntity.setPosting_order_no(posting_order_no);
				pEntity.setNrs(nrs);
				pEntity.setMovedate(movedate);
				iPostingEntityService.update(pEntity);

			}

			upload_status = true;
			logger.info("Posting Excel File Read Successfully in DataBase");
			
			
		}

		catch (FileNotFoundException e) {
			upload_status = false;
			logger.info(e.getMessage() + " " + e.getCause());
			throw new MultipartException("Constraints Violated");
		}
		return upload_status;

	}

	/****************************************************************************************************************************/
	public String uploadSalaryDocument(MultipartFile file, String loginId, String gsno, String date) {

		SalaryFileEntity objfileUpload = new SalaryFileEntity();
		String Salary_File="";

		try {
	
			Salary_File = uploadSalaryDocuments(loginId, file, gsno, date);
			
			// objfileUpload.setUploadTextDocument(MOBILE_DOCUMENT !=null ? MOBILE_DOCUMENT
			// : null);
			// objfileUpload.setLoginId(loginId);
			// iDataGenericService.update(objfileUpload);
			return Salary_File;

		} catch (Exception e) {
			// TODO: handle exception
			// ReportOfUploadFile(RIGHT_NUMBER_DATA,WROUND_NUMBER_DATA,DUPLICATE_NUMBER_DATA);
			System.out.println("e  " + e);
			logger.info("######## Error in File Upload ########" + e.getMessage());
			return Salary_File;
		}
	}
	
	private String uploadSalaryDocuments(String loginId, MultipartFile multipartFile, String gsno, String date) {

		String newDocName = "";
		String fileNamePrefix = "";
		String MONTH = "";
		String YEAR = "";
		String NEW_FILE_NAME = "";

		System.out.println("YYY-MM-DD : " + date);
		String[] strs = date.split("-");
		YEAR = strs[0];
		System.out.println(YEAR);
		MONTH = strs[1];
		System.out.println(MONTH);

		try {
			if (!multipartFile.isEmpty()) {
				File doc = new File(SALARY_PATH + "/" + YEAR + "/" + MONTH + "/");
				logger.info("DOUCMENT PATH : " + doc);
				if (!doc.exists()) {
					logger.info("Directory Not Exists : " + doc);
					doc.mkdirs();
				}

				String fileName = "";
				String s = multipartFile.getOriginalFilename();
				String[] words = s.split("\\s+");
				for (int i = 0; i < words.length; i++) {
					fileName = fileName + words[i].replaceAll(" ", "");
					System.out.println("Trim File Name" + fileName);
				}

				String destination = fileName.substring(0, fileName.indexOf(".pdf"));
				NEW_FILE_NAME = "MPS_Y0000000_GS" + gsno + "H_" + MONTH + "_" + YEAR;
				System.out.println("Writing " + destination);
				String newfile = destination.replaceAll(destination, NEW_FILE_NAME);

				NEW_FILE_NAME = newfile + ".pdf";
				System.out.println("*********** : " + NEW_FILE_NAME);

				String folder_name = SALARY_PATH + "/" + YEAR + "/" + MONTH + "/" + NEW_FILE_NAME;
				logger.info("Orignal Folder Name : " + folder_name);
				File file = new File(folder_name);
				if (fileNamePrefix != null) {

					newDocName = fileNamePrefix + NEW_FILE_NAME;
					logger.info("New Document Name With FileNamePrefix : " + newDocName);
				} else {
					newDocName = NEW_FILE_NAME;
					logger.info("New Document Name Without FileNamePrefix : " + newDocName);

				}
				String newPath = SALARY_PATH + "/" + YEAR + "/" + MONTH + "/" + newDocName;
				logger.info("Rename File : " + newPath);
				File newFile = new File(newPath);

				if (!file.isDirectory()) {
					file.renameTo(newFile);
				}
				byte[] bytes = multipartFile.getBytes();
				Path path = Paths.get(newPath);
				logger.info("path : " + path);
				Files.write(path, bytes);
			}
		} catch (IOException e) {
			logger.info("IOException : " + e.getMessage());
			newDocName = "";
		}
		return newDocName;
	}

	

	/****************************************************************************************************************************/
	public String uploadForm16Document(MultipartFile file, String loginId, String gsno, String from,String to) {

		String Form16_File="";
		SalaryFileEntity objfileUpload = new SalaryFileEntity();
		
		try {

			 Form16_File = uploadForm16Documents(loginId, file, gsno, from,to);
			// objfileUpload.setUploadTextDocument(MOBILE_DOCUMENT !=null ? MOBILE_DOCUMENT
			// : null);
			// objfileUpload.setLoginId(loginId);
			// iDataGenericService.update(objfileUpload);
			 System.out.println("Create Form 16 File" + Form16_File);
			return Form16_File;
		} catch (Exception e) {
			// TODO: handle exception
			// ReportOfUploadFile(RIGHT_NUMBER_DATA,WROUND_NUMBER_DATA,DUPLICATE_NUMBER_DATA);
			System.out.println("e  " + e);
			logger.info("######## Error in File Upload ########" + e.getMessage());
			return Form16_File;
		}
	}
	
	
	
	private String uploadForm16Documents(String loginId, MultipartFile multipartFile, String gsno, String from,String to) {

		String newDocName = "";
		String fileNamePrefix = "";
		String MONTH = "";
		String YEAR = "";
		String NEW_FILE_NAME = "";

		String FROM="";
		String TO="";
		String GSNO= "";
		//Form16_Y0000000_GS180323H_2015_2016
		
//		System.out.println("YYY-MM-DD : " + date);
//		String[] strs = date.split("-");
//		YEAR = strs[0];
//		System.out.println(YEAR);
//		MONTH = strs[1];
//		System.out.println(MONTH);

		FROM = from;
		TO = to;
		GSNO = gsno;
		
		
		try {
			if (!multipartFile.isEmpty()) {
				File doc = new File(POLICY_PATH + "/");
				logger.info("DOUCMENT PATH : " + doc);
				if (!doc.exists()) {
					logger.info("Directory Not Exists : " + doc);
					doc.mkdirs();
				}

				String fileName = "";
				String s = multipartFile.getOriginalFilename();
				String[] words = s.split("\\s+");
				for (int i = 0; i < words.length; i++) {
					fileName = fileName + words[i].replaceAll(" ", "");
					System.out.println("Trim File Name" + fileName);
				}

				String destination = fileName.substring(0, fileName.indexOf(".pdf"));
				NEW_FILE_NAME = "Form16_Y0000000_GS" + GSNO + "H_" + FROM + "_" + TO;
				System.out.println("Writing " + destination);
				String newfile = destination.replaceAll(destination, NEW_FILE_NAME);

				NEW_FILE_NAME = newfile + ".pdf";
				System.out.println("*********** : " + NEW_FILE_NAME);

				String folder_name = FORM16_PATH + "/" + NEW_FILE_NAME;
				logger.info("Orignal Folder Name : " + folder_name);
				File file = new File(folder_name);
				if (fileNamePrefix != null) {

					newDocName = fileNamePrefix + NEW_FILE_NAME;
					logger.info("New Document Name With FileNamePrefix : " + newDocName);
				} else {
					newDocName = NEW_FILE_NAME;
					logger.info("New Document Name Without FileNamePrefix : " + newDocName);

				}
				String newPath = FORM16_PATH + "/" + newDocName;
				logger.info("Rename File : " + newPath);
				File newFile = new File(newPath);

				if (!file.isDirectory()) {
					file.renameTo(newFile);
				}
				byte[] bytes = multipartFile.getBytes();
				Path path = Paths.get(newPath);
				logger.info("path : " + path);
				Files.write(path, bytes);
			}
		} catch (IOException e) {
			logger.info("IOException : " + e.getMessage());
			newDocName = "";
		}
		return newDocName;
	}

	
	
	
	/****************************************************************************************************************************/
	public String uploadPolicyDocument(MultipartFile file, String loginId, String policyType) {

		String Policy_File="";
		
		SalaryFileEntity objfileUpload = new SalaryFileEntity();

		try {

			 Policy_File = uploadPolicyDocuments(loginId, file,policyType);
			// objfileUpload.setUploadTextDocument(MOBILE_DOCUMENT !=null ? MOBILE_DOCUMENT
			// : null);
			// objfileUpload.setLoginId(loginId);
			// iDataGenericService.update(objfileUpload);
			 return Policy_File;
		} catch (Exception e) {
			// TODO: handle exception
			// ReportOfUploadFile(RIGHT_NUMBER_DATA,WROUND_NUMBER_DATA,DUPLICATE_NUMBER_DATA);
			System.out.println("e  " + e);
			logger.info("######## Error in File Upload ########" + e.getMessage());
			return Policy_File;
		}
	}
	
	
	
	private String uploadPolicyDocuments(String loginId, MultipartFile multipartFile , String policyType) {

		String newDocName = "";
		String fileNamePrefix = "";
		String MONTH = "";
		String YEAR = "";
		String NEW_FILE_NAME = "";
		String POLICY_NAME ="";

		
		//Form16_Y0000000_GS180323H_2015_2016
		
//		System.out.println("YYY-MM-DD : " + date);
//		String[] strs = date.split("-");
//		YEAR = strs[0];
//		System.out.println(YEAR);
//		MONTH = strs[1];
//		System.out.println(MONTH);

		POLICY_NAME = policyType;
	
		
		try {
			if (!multipartFile.isEmpty()) {
				File doc = new File(POLICY_PATH +"/");
				logger.info("DOUCMENT PATH : " + doc);
				if (!doc.exists()) {
					logger.info("Directory Not Exists : " + doc);
					doc.mkdirs();
				}

				String fileName = "";
				String s = multipartFile.getOriginalFilename();
				String[] words = s.split("\\s+");
				for (int i = 0; i < words.length; i++) {
					fileName = fileName + words[i].replaceAll(" ", "");
					System.out.println("Trim File Name" + fileName);
				}

				String destination = fileName.substring(0, fileName.indexOf(".pdf"));
				
				System.out.println("Writing " + destination);
				String newfile = destination.replaceAll(destination, POLICY_NAME);

				NEW_FILE_NAME = newfile + ".pdf";
				System.out.println("*********** : " + NEW_FILE_NAME);

				String folder_name = POLICY_PATH + "/" + NEW_FILE_NAME;
				logger.info("Orignal Folder Name : " + folder_name);
				File file = new File(folder_name);
				if (fileNamePrefix != null) {

					newDocName = fileNamePrefix + NEW_FILE_NAME;
					logger.info("New Document Name With FileNamePrefix : " + newDocName);
				} else {
					newDocName = NEW_FILE_NAME;
					logger.info("New Document Name Without FileNamePrefix : " + newDocName);

				}
				String newPath = POLICY_PATH + "/" + newDocName;
				logger.info("Rename File : " + newPath);
				File newFile = new File(newPath);

				if (!file.isDirectory()) {
					file.renameTo(newFile);
				}
				byte[] bytes = multipartFile.getBytes();
				Path path = Paths.get(newPath);
				logger.info("path : " + path);
				Files.write(path, bytes);
			}
		} catch (IOException e) {
			logger.info("IOException : " + e.getMessage());
			newDocName = "";
		}
		return newDocName;
	}
	
	
	
	public String uploadCourseDocument(MultipartFile file, String loginId, String courseType) {

		String Course_file="";
		SalaryFileEntity objfileUpload = new SalaryFileEntity();

		try {

			Course_file = uploadCourseDocuments(loginId, file,courseType);
			// objfileUpload.setUploadTextDocument(MOBILE_DOCUMENT !=null ? MOBILE_DOCUMENT
			// : null);
			// objfileUpload.setLoginId(loginId);
			// iDataGenericService.update(objfileUpload);
			return Course_file;
		} catch (Exception e) {
			// TODO: handle exception
			// ReportOfUploadFile(RIGHT_NUMBER_DATA,WROUND_NUMBER_DATA,DUPLICATE_NUMBER_DATA);
			System.out.println("e  " + e);
			logger.info("######## Error in File Upload ########" + e.getMessage());
			return Course_file;
		}
	}
	
	
	
	private String uploadCourseDocuments(String loginId, MultipartFile multipartFile , String courseType) {

		String newDocName = "";
		String fileNamePrefix = "";
		String MONTH = "";
		String YEAR = "";
		String NEW_FILE_NAME = "";
		String COURSE_NAME ="";
		String DIRECTORY_NAME="";


		COURSE_NAME = courseType;
		
		if (COURSE_NAME.equals("COURSE_RESULT") || COURSE_NAME=="COURSE_RESULT") {
			DIRECTORY_NAME =  COURSE_RESULT_PATH;
		} else {
			DIRECTORY_NAME =  UPCOMING_COURSE_PATH;
		}
	
			System.out.println("Final Course Directory*********** " + DIRECTORY_NAME);
		
		try {
			if (!multipartFile.isEmpty()) {
				File doc = new File(DIRECTORY_NAME +"/");
				logger.info("DOUCMENT PATH : " + doc);
				if (!doc.exists()) {
					logger.info("Directory Not Exists : " + doc);
					doc.mkdirs();
				}

				String fileName = "";
				String s = multipartFile.getOriginalFilename();
				String[] words = s.split("\\s+");
				for (int i = 0; i < words.length; i++) {
					fileName = fileName + words[i].replaceAll(" ", "");
					System.out.println("Trim File Name" + fileName);
				}

				String destination = fileName.substring(0, fileName.indexOf(".pdf"));
				
				System.out.println("Writing " + destination);
				String newfile = destination.replaceAll(destination, COURSE_NAME);

				NEW_FILE_NAME = newfile + ".pdf";
				System.out.println("*********** : " + NEW_FILE_NAME);

				String folder_name = DIRECTORY_NAME + "/" + NEW_FILE_NAME;
				logger.info("Orignal Folder Name : " + folder_name);
				File file = new File(folder_name);
				if (fileNamePrefix != null) {

					newDocName = fileNamePrefix + NEW_FILE_NAME;
					logger.info("New Document Name With FileNamePrefix : " + newDocName);
				} else {
					newDocName = NEW_FILE_NAME;
					logger.info("New Document Name Without FileNamePrefix : " + newDocName);

				}
				String newPath = DIRECTORY_NAME + "/" + newDocName;
				logger.info("Rename File : " + newPath);
				File newFile = new File(newPath);

				if (!file.isDirectory()) {
					file.renameTo(newFile);
				}
				byte[] bytes = multipartFile.getBytes();
				Path path = Paths.get(newPath);
				logger.info("path : " + path);
				Files.write(path, bytes);
			}
		} catch (IOException e) {
			logger.info("IOException : " + e.getMessage());
			newDocName = "";
		}
		return newDocName;
	}

	
	
	
	
}
