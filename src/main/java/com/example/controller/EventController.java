package com.example.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.EventEntity;
import com.example.service.IGenericService;

@RestController
@CrossOrigin
@RequestMapping(value = "/querydata")
public class EventController {

	private static final Logger logger = LoggerFactory.getLogger(EventController.class);
	
	 @Value("${header}")
	 private String HEADER;
	 
	 @Value("${footer}")
	 private String FOOTER;
	 
	 @Value("${startTag}")
	 private String STARTTAG;
	 
	 @Value("${stopTag}")
	 private String STOPTAG;
	 
	 
	@Autowired
	private IGenericService<EventEntity> iEventEntityService;

	/************************* Get Event on A Mobile ***********************/
	@GetMapping(value = "/get/event")
	public EventEntity getEvent() {
		List<EventEntity> eventList = iEventEntityService.fetch(new EventEntity(), "order by datetime desc");
		String str="";

		// show last five event
		for (int i = 0; i < 4; i++) {
			if (i == 0) {
				str =STARTTAG+ eventList.get(i).getMessage()+STOPTAG;
			} else {
				str = str + STARTTAG + eventList.get(i).getMessage()+STOPTAG;
			}

			logger.info(str);
			
		}
		
		//  onmouseover=\"this.stop();\" onmouseout=\"this.start();\"
		String head=HEADER;
		
		String footer=FOOTER;
		str=head+str+footer;
		logger.info("Total " + eventList.size() + "Found");
		// str="Posting Orders all trades upto Q/E Dec 2017,"
		// + " uploaded in the BRO Website. \n \n DPC for U/R Cat scheduled on Jan
		// 2018.\n\n MSL upto Sep 2017 uploaded\n\n CE conference scheduled on 2nd week
		// of Dec 2017";
		EventEntity event = new EventEntity();
		event.setMessage(str);
		event.setId(1);
		return event;
	}

	/************************* Get Event on A Pannel ***********************/
	@GetMapping(value = "/get/event/pannel")
	public ResponseEntity<?> getPannelEvent() {
		List<EventEntity> eventList = iEventEntityService.fetch(new EventEntity(), "order by datetime desc");
		logger.info("Total " + eventList.size() + "Found");
		return new ResponseEntity<>(eventList, HttpStatus.OK);
	}

	// @PostMapping(value = "/save/event")
	// public ResponseEntity<?> saveEventy(@RequestBody EventEntity objEventEntity)
	// {
	//
	// objEventEntity.setDatetime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss
	// a").format(new Date()).toString());
	// iEventEntityService.save(objEventEntity);
	// logger.info("Save Found " + objEventEntity.getId());
	// return new ResponseEntity<>(HttpStatus.OK);
	// }

	@PostMapping(value = "/save/event")
	public ResponseEntity<?> saveEventy(@RequestBody EventEntity objEventEntity) {

		

		objEventEntity.setDatetime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(new Date()).toString());
		objEventEntity.setFlag(true);
		iEventEntityService.save(objEventEntity);
		logger.info("Evenet Saved");
		
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/news/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteNews(@PathVariable("id") String id) {

		EventEntity objAddNewsEntity = iEventEntityService.find(new EventEntity(), "Where id='" + id + "'");
		if (objAddNewsEntity != null && !objAddNewsEntity.equals("")) {
			iEventEntityService.delete(objAddNewsEntity);
			logger.info(" #### News Deleted Successfully For newsId #### " + id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			logger.info(" #### News Not Deleted For newsId #### " + id);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	@PutMapping(value = "/update/news")
	// @CacheEvict(cacheNames = "objAddNewsEntity")
	public ResponseEntity<?> updateNews(@RequestBody EventEntity objAddNewsEntity) {

		if (objAddNewsEntity != null && !objAddNewsEntity.equals("")) {
			logger.info(" #### News Update Successfully #### ");
			objAddNewsEntity.setDatetime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(new Date()).toString());
			iEventEntityService.update(objAddNewsEntity);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			logger.info(" #### Error in News Update #### ");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

}
