package com.example.controller;

import com.example.entity.Photos;
import com.example.service.IGenericService;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@CrossOrigin
public class ImagesController
{
  private static final Logger logger = LoggerFactory.getLogger(ImagesController.class);
  @Autowired
  private IGenericService<Photos> iPhotosService;
  @Value("${filepath.uploadpath}")
  private String UPLOADED_FOLDER = "D:/";
  String prefix = "";
  
  @GetMapping({"/querydata/photodetails"})
  public ResponseEntity<List<Photos>> getPhotos(@RequestParam(value="gsno", required=true) String gsno)
  {
    logger.info(gsno);
    List<Photos> listPhotos = this.iPhotosService.fetch(new Photos(), " where gsno = '" + gsno + "'");
    if (listPhotos == null) {
      return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
    return new ResponseEntity(listPhotos, HttpStatus.OK);
  }
  
  @PostMapping({"/querydata/photouploa"})
  public ResponseEntity<Photos> savePhotosDetails(Photos photos, @RequestParam(value="gsno", required=true) String gsno, @RequestPart(value="photo_self", required=false) MultipartFile photo_self, @RequestPart(value="photo_wife", required=false) MultipartFile photo_wife, @RequestPart(value="photo_joint", required=false) MultipartFile photo_joint, HttpServletRequest request)
  {
    logger.info("photo upload............");
    try
    {
      String fileName = "";
      Photos photos1 = (Photos)this.iPhotosService.find(new Photos(), " where gsno ='" + gsno + "'");
      String save = this.UPLOADED_FOLDER.substring(this.UPLOADED_FOLDER.lastIndexOf("/"), this.UPLOADED_FOLDER.length());
      String fetchFile = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + save + "/";
      if (photos1 == null)
      {
        photos.setGsno(gsno);
        this.iPhotosService.save(photos);
        photos1 = (Photos)this.iPhotosService.find(new Photos(), " where gsno ='" + gsno + "'");
      }
      logger.info(" obj  " + photos1);
      if (photo_self != null)
      {
        this.prefix = "self";
        fileName = uploadDocuments(gsno, photo_self, this.prefix);
        photos1.setImage_url(fetchFile + "/" + this.prefix + "/" + gsno + "/" + fileName);
        photos1.setPhoto_self(fileName);
      }
      if (photo_wife != null)
      {
        this.prefix = "wife";
        fileName = uploadDocuments(gsno, photo_wife, this.prefix);
        photos1.setImage_url(fetchFile + "/" + this.prefix + "/" + gsno + "/" + fileName);
        photos1.setPhoto_wife(fileName);
      }
      if (photo_joint != null)
      {
        this.prefix = "joint";
        fileName = uploadDocuments(gsno, photo_joint, this.prefix);
        photos1.setImage_url(fetchFile + "/" + this.prefix + "/" + gsno + "/" + fileName);
        photos1.setPhoto_joint(fileName);
      }
      this.iPhotosService.update(photos1);
    }
    catch (Exception e)
    {
      logger.info(e.getMessage());
      return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity(HttpStatus.CREATED);
  }
  
  private String uploadDocuments(String gsno, MultipartFile multipartFile, String fileNamePrefix)
    throws Exception
  {
    String newDocName = "";
    String newPath = "";
    try
    {
      if ((gsno != null) || (!multipartFile.isEmpty()))
      {
        File doc = new File(this.UPLOADED_FOLDER + "/" + fileNamePrefix + "/" + gsno + "/");
        if (!doc.exists())
        {
          logger.info(" Dir " + gsno + " NOT Exists");
          doc.mkdirs();
        }
        String fileName = "";
        
        String s = multipartFile.getOriginalFilename();
        String[] words = s.split("\\s+");
        for (int i = 0; i < words.length; i++) {
          fileName = fileName + words[i].replaceAll(" ", "");
        }
        String folder_name = this.UPLOADED_FOLDER + "/" + fileNamePrefix + "/" + gsno + "/" + fileName;
        
        File file = new File(folder_name);
        if (fileNamePrefix != null) {
          newDocName = fileNamePrefix + "_" + fileName;
        } else {
          newDocName = fileName;
        }
        newPath = this.UPLOADED_FOLDER + "/" + fileNamePrefix + "/" + gsno + "/" + newDocName;
        
        File newFile = new File(newPath);
        if (!file.isDirectory()) {
          file.renameTo(newFile);
        }
        byte[] bytes = multipartFile.getBytes();
        Path path = Paths.get(newPath, new String[0]);
        logger.info("path" + path);
        Files.write(path, bytes, new OpenOption[0]);
      }
    }
    catch (IOException e)
    {
      logger.info("IOException : " + e.getMessage());
    }
    return newDocName;
  }
}
