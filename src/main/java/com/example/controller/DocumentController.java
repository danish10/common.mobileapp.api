package com.example.controller;

import com.example.bean.ApiBean;
import com.example.bean.ErrorResponse;
import com.example.entity.UserLogin;
import com.example.service.FileSearchService;
import com.example.service.IGenericService;
import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class DocumentController
{
  private static final Logger logger = LoggerFactory.getLogger(DocumentController.class);
  @Value("${salary.path}")
  private String SALARY_PATH;
  
  @Value("${policy.path}")
  private String POLICY_PATH;
  
  @Value("${form16.path}")
  private String FORM16_PATH;
  
  @Value("${courseResult.path}")
  private String COURSE_RESULT_PATH;
  
  @Value("${upcomingCourse.path}")
  private String UPCOMING_COURSE_PATH;
  
  @Autowired
  private FileSearchService fileSearchService;
  @Autowired
  private IGenericService<UserLogin> iDataGenericService;
  
  
  @PostMapping({"/querydata/salary"})
  public ResponseEntity<?> getSalaryDetails(ApiBean apiBean, HttpServletRequest request, @RequestParam(value="gsno", required=true) String gsno, @RequestParam(value="year", required=true) String year, @RequestParam(value="month", required=true) String month)
  {
    logger.info(gsno);
    
    System.out.println("salary ----"+SALARY_PATH+" "+POLICY_PATH+"   "+FORM16_PATH);
    String path = this.SALARY_PATH + "/" + year + "/" + month;
    logger.info("Path : " + path);
    String save = this.SALARY_PATH.substring(this.SALARY_PATH.lastIndexOf("/"), this.SALARY_PATH.length());
    logger.info("save : " + save);
    String name = this.fileSearchService.searchFile(new File(path), gsno, year, month);
    System.out.println("\nFound " + name + " result!\n");
    apiBean.setGsno(gsno);
    if ((name.isEmpty()) || (name == null))
    {
      String message = "No Document found! ";
      logger.info(message);
      return new ResponseEntity(new ErrorResponse(HttpStatus.NO_CONTENT, "0", message), HttpStatus.OK);
    }
    UserLogin userLogin = (UserLogin)this.iDataGenericService.find(new UserLogin(), " where gsno = '" + gsno + "'");
    String fetchFile = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + save + "/" + year.toLowerCase() + "/" + month.toLowerCase() + "/";

 apiBean.setId((int) userLogin.getId());
    apiBean.setSalaryUrl(fetchFile + name);
logger.info(fetchFile+" file and name  "+name);    
    return new ResponseEntity(apiBean, HttpStatus.OK);
  }
  
  
  
  @PostMapping({"/querydata/policy"})
  public ResponseEntity<ApiBean> getPolicyDoc(ApiBean apiBean, HttpServletRequest request)
  {
	  System.out.println("policy ----"+SALARY_PATH+" "+POLICY_PATH+"   "+FORM16_PATH);
    List<String> list = this.fileSearchService.searchAllPathFiles(new File(this.POLICY_PATH));
    System.out.println("\nFound " + list.size() + " result!\n");
    String save = this.POLICY_PATH.substring(this.POLICY_PATH.lastIndexOf("/"), this.POLICY_PATH.length());
    if (list.size() == 0)
    {
      System.out.println("\nNo result found!");
      apiBean.setDocument(list);
      return new ResponseEntity(apiBean, HttpStatus.NO_CONTENT);
    }
    String fetchFile = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + save + "/";
    List<String> updateList = new ArrayList();
    for (String name : list)
    {
      System.out.println("Found : " + name);
      updateList.add(fetchFile + name);
    }
    apiBean.setDocument(updateList);
    
    return new ResponseEntity(apiBean, HttpStatus.OK);
  }
  
  
  
  @PostMapping({"/querydata/form16"})
  public ResponseEntity<?> getForm16Doc(ApiBean apiBean, HttpServletRequest request, @RequestParam(value="gsno", required=true) String gsno, @RequestParam(value="year", required=true) String year)
  {
	  System.out.println("form 16 ----"+ gsno +" "+FORM16_PATH);
	  
    String save = this.FORM16_PATH.substring(this.FORM16_PATH.lastIndexOf("/"), this.FORM16_PATH.length());
    logger.info("save : " + save);
    String name = this.fileSearchService.searchForm16(new File(this.FORM16_PATH),gsno, year);
    System.out.println("\n Found " + name + " result!\n");
     if ((name.isEmpty()) || (name == null))
    {
      String message = "No Document found! ";
      logger.info(message);
      return new ResponseEntity(new ErrorResponse(HttpStatus.NO_CONTENT, "0", message), HttpStatus.OK);
    }
    UserLogin userLogin = (UserLogin)this.iDataGenericService.find(new UserLogin(), " where gsno = '" + gsno + "'");
    String fetchFile = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + save + "/";
    logger.info("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh  "+fetchFile);
    apiBean.setId((int) userLogin.getId());
    apiBean.setForm16Url(fetchFile + name);
   System.out.println("ffffffffffffffffffffff "+fetchFile+"   "+name); 
    return new ResponseEntity(apiBean, HttpStatus.OK);
  }
  
  
  @GetMapping({"/querydata/course/result"})
  public ResponseEntity<ApiBean> getCourseDoc(ApiBean apiBean, HttpServletRequest request)
  {
	  System.out.println("course ----"+COURSE_RESULT_PATH);
    List<String> list = this.fileSearchService.searchAllPathFiles(new File(this.COURSE_RESULT_PATH));
    System.out.println("\nFound " + list.size() + " result!\n");
    String save = this.COURSE_RESULT_PATH.substring(this.COURSE_RESULT_PATH.lastIndexOf("/"), this.COURSE_RESULT_PATH.length());
    logger.info("save dir "+save);
    if (list.size() == 0)
    {
      System.out.println("\nNo result found!");
      apiBean.setDocument(list);
      return new ResponseEntity(apiBean, HttpStatus.NO_CONTENT);
    }
    String fetchFile = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + save + "/";
//    List<String> updateList = new ArrayList();
//    for (String name : list)
//    {
//      System.out.println("Found : " + name);
//      updateList.add(fetchFile + name);
//    }
    apiBean.setId(1);
    logger.info("fetchFile "+fetchFile);
    fetchFile=fetchFile +list.get(0);
    apiBean.setCourseResult(fetchFile);
    logger.info("fetchFile with name"+fetchFile);
    return new ResponseEntity(apiBean, HttpStatus.OK);
  }
  
  
  @GetMapping({"/querydata/course/upcoming"})
  public ResponseEntity<ApiBean> getCourseResult(ApiBean apiBean, HttpServletRequest request)
  {
	  System.out.println("course ----"+UPCOMING_COURSE_PATH+" "+POLICY_PATH+"   "+FORM16_PATH);
    List<String> list = this.fileSearchService.searchAllPathFiles(new File(this.UPCOMING_COURSE_PATH));
    System.out.println("\nFound " + list.size() + " result!\n");
    String save = this.UPCOMING_COURSE_PATH.substring(this.UPCOMING_COURSE_PATH.lastIndexOf("/"), this.UPCOMING_COURSE_PATH.length());
    if (list.size() == 0)
    {
      System.out.println("\nNo result found!");
      apiBean.setId(1);
      apiBean.setDocument(list);
      return new ResponseEntity(apiBean, HttpStatus.NO_CONTENT);
    }
    String fetchFile = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + save + "/";
//    List<String> updateList = new ArrayList();
//    for (String name : list)
//    {
//      System.out.println("Found : " + name);
//      updateList.add(fetchFile + name);
//    }
    fetchFile=fetchFile +list.get(0);
    apiBean.setId(1);
    apiBean.setUpcomingCourse(fetchFile);
   // apiBean.setCoursedocument(updateList);
    
    return new ResponseEntity(apiBean, HttpStatus.OK);
  }
  
  
}
