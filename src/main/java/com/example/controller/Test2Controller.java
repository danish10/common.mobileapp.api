package com.example.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.Test2;
import com.example.service.IGenericService;

@RestController
@CrossOrigin
@RequestMapping(value = "/test2")
public class Test2Controller {

	private static final Logger logger = LoggerFactory.getLogger(Test2Controller.class);

	@Autowired
	private IGenericService<Test2> iTest2Service;

	@PostMapping()
	public ResponseEntity<?> saveTest2(@RequestBody Test2 objTest2) {
		iTest2Service.save(objTest2);
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PutMapping()
	public ResponseEntity<?> updateTest2(@RequestBody Test2 objTest2) {
		iTest2Service.update(objTest2);
		logger.info("Data update successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@DeleteMapping()
	public ResponseEntity<?> deleteTest2(@RequestParam(value = "id", required = true) String id) {
		Test2 objTest2 = iTest2Service.find(new Test2(), "where id = " + id);
		if (objTest2 == null) {
			return new ResponseEntity<>(objTest2, HttpStatus.NO_CONTENT);
		}
		iTest2Service.delete(objTest2);
		logger.info("Data deleted successfully !");
		return new ResponseEntity<>(HttpStatus.GONE);
	}

	@GetMapping()
	public ResponseEntity<?> findTest2(@RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "text1", required = false) String text1) {

		if (id == null && text1 == null) {
			return new ResponseEntity<>(iTest2Service.fetch(new Test2()), HttpStatus.OK);
		}

		Test2 objTest2 = null;
		if (id != null && text1 == null) {
			objTest2 = iTest2Service.find(new Test2(), "where id = " + id);
		} else if (id == null && text1 != null) {
			objTest2 = iTest2Service.find(new Test2(), " where text2 = '" + text1 + "'");
		} else if (id != null && text1 != null) {
			objTest2 = iTest2Service.find(new Test2(), " where id = " + id + " and text2 = '" + text1 + "'");
		}

		if (objTest2 == null) {
			return new ResponseEntity<>(objTest2, HttpStatus.NO_CONTENT);
		}

		logger.info("Data found! ");
		return new ResponseEntity<>(objTest2, HttpStatus.OK);
	}
}
