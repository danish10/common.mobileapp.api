package com.example.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.example.bean.ErrorResponse;
import com.example.file.service.UploadFileService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RestController
@CrossOrigin
public class UploadDocumentController {

	private static final Logger logger = LoggerFactory.getLogger(UploadDocumentController.class);

	@Autowired
	private UploadFileService fileService;

	String message = "";
	String fileName = "";

	List<String> files = new ArrayList<String>();
	
	@GetMapping("/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> getFile(@PathVariable String filename) {
		Resource file = fileService.loadFile(filename);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}

	@RequestMapping(value = "/upload/posting/file", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> uploadPDFFile(@RequestPart("file") MultipartFile file,
			@RequestParam(value = "loginId") String loginId) {

		System.out.println(loginId);
		
		fileName = fileService.uploadPostingDocument(file, loginId);
		

		if (fileName.equals("true")) {
			message = "Posting File With " + fileName + " Status Upload Successfully";
			return new ResponseEntity(fileName, HttpStatus.OK);
			
		} else {
			message = "Posting File Not Found";
			return new ResponseEntity(new ErrorResponse(HttpStatus.NOT_FOUND, "1", message), HttpStatus.OK);
		}

	}

	
	
	@RequestMapping(value = "/upload/salary/file", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> uploadPDFFile(@RequestPart("file") MultipartFile file,
			@RequestParam(value = "loginId") String loginId, @RequestParam(value = "gsno", required = true) String gsno,
			@RequestParam(value = "date", required = true) String date) {

		System.out.println(loginId);
		System.out.println(gsno);
		System.out.println(date);

		fileName = fileService.uploadSalaryDocument(file, loginId, gsno, date);

		if (fileName.equals("") && fileName == null) {
			message = "Salary File Not Found";
			return new ResponseEntity(new ErrorResponse(HttpStatus.NOT_FOUND, "1", message), HttpStatus.OK);
		} else {
			message = "Salary File Upload Successfully For : " + gsno + " & FileName " + fileName;
			return new ResponseEntity(new ErrorResponse(HttpStatus.OK, "0", message), HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/upload/form16/file", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> uploadFORM16File(@RequestPart("file") MultipartFile file,
			@RequestParam(value = "loginId") String loginId, @RequestParam(value = "gsno", required = true) String gsno,
			@RequestParam(value = "from", required = true) String from,
			@RequestParam(value = "to", required = true) String to) {

		System.out.println(loginId);
		System.out.println(gsno);
		System.out.println(from);
		System.out.println(to);

		fileName = fileService.uploadForm16Document(file, loginId, gsno, from, to);

		if (fileName.equals("") && fileName == null) {
			message = "Form16 File Not Found";
			return new ResponseEntity(new ErrorResponse(HttpStatus.NOT_FOUND, "1", message), HttpStatus.OK);
		} else {
			message = "Form16 File Upload Successfully For : " + gsno + " & From " + from + " To " + to
					+ " & FileName : " + fileName;
			return new ResponseEntity(new ErrorResponse(HttpStatus.OK, "0", message), HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/upload/policy/file", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> uploadPOLICYFile(@RequestPart("file") MultipartFile file,
			@RequestParam(value = "loginId") String loginId,
			@RequestParam(value = "policyType", required = true) String policyType) {

		System.out.println(loginId);
		System.out.println(policyType);

		fileName = fileService.uploadPolicyDocument(file, loginId, policyType);
		if (fileName.equals("") && fileName == null) {
			message = "Policy File Not Found";
			return new ResponseEntity(new ErrorResponse(HttpStatus.NOT_FOUND, "1", message), HttpStatus.OK);
		} else {
			message = "Policy File Upload Successfully For : " + policyType + " & FileName : " + fileName;
			;
			return new ResponseEntity(new ErrorResponse(HttpStatus.OK, "0", message), HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/upload/course/file", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> uploadCOURSEFile(@RequestPart("file") MultipartFile file,
			@RequestParam(value = "loginId") String loginId,
			@RequestParam(value = "courseType", required = true) String courseType) {

		System.out.println(loginId);
		System.out.println(courseType);

		fileName = fileService.uploadCourseDocument(file, loginId, courseType);

		if (fileName.equals("") && fileName == null) {
			message = "Course File Not Found";
			return new ResponseEntity(new ErrorResponse(HttpStatus.NOT_FOUND, "1", message), HttpStatus.OK);
		} else {
			message = "Course File Upload Successfully For : " + courseType + " & FileName : " + fileName;
			;
			return new ResponseEntity(new ErrorResponse(HttpStatus.OK, "0", message), HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/processwithfiles", method = RequestMethod.POST)
	public void processWithFiles(@RequestParam("file") final MultipartFile[] files)
			throws JsonParseException, JsonMappingException, IOException {

		// MetaData document = objectMapper.readValue(metaData, MetaData.class);
		for (MultipartFile f : files) {
			File file = new File(files + f.getOriginalFilename());
			System.out.println("MMMMMMMMMMMMMM " + file);
			f.transferTo(file);

		}

	}

	@RequestMapping(value = "/upload/multiple/salary/file", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> uploadMultipleSalaryFile(@RequestParam("file") MultipartFile[] files) throws IOException {

		System.out.println("1");
		String message = "";
		String UPLOADED_FOLDER = "E:/GREF/apidocument/MultipleFiles";
		File dir = new File(UPLOADED_FOLDER);

		System.out.println("2");

		for (int i = 0; i < files.length; i++) {
			MultipartFile file = files[i];

			try {
				byte[] bytes = file.getBytes();

				if (!dir.exists())
					dir.mkdirs();

				File uploadFile = new File(dir.getAbsolutePath() + File.separator + file.getOriginalFilename());
				BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(uploadFile));
				outputStream.write(bytes);
				outputStream.close();

				message = "You successfully uploaded file=" + file.getOriginalFilename();
			} catch (Exception e) {
				message = "Failed to upload " + file.getOriginalFilename() + " " + e.getMessage();
			}
		}

		// for(MultipartFile f : uploadedFiles) {
		// File file = new File(uploadingdir + f.getOriginalFilename());
		// System.out.println("MMMMMMMMMMMMMM " + file);
		// f.transferTo(file);
		// }
		return new ResponseEntity(new ErrorResponse(HttpStatus.OK, "0", message), HttpStatus.OK);
	}

	// @RequestMapping(value = "/uploadMultipleFiles ", method =RequestMethod.POST)
	// public String uploadMultipleFiles(@RequestParam("file") MultipartFile[]
	// files) {
	//
	// System.out.println("Enter into dddd");
	//
	// String UPLOADED_FOLDER = "E:/GREF/apidocument/MultipleFiles";
	//
	//
	// String status = "";
	// File dir = new File(UPLOADED_FOLDER);
	// for (int i = 0; i < files.length; i++) {
	// MultipartFile file = files[i];
	//
	// try {
	// byte[] bytes = file.getBytes();
	//
	// if (!dir.exists())
	// dir.mkdirs();
	//
	// File uploadFile = new File(dir.getAbsolutePath()
	// + File.separator + file.getOriginalFilename());
	// BufferedOutputStream outputStream = new BufferedOutputStream(
	// new FileOutputStream(uploadFile));
	// outputStream.write(bytes);
	// outputStream.close();
	//
	// status = status + "You successfully uploaded file=" +
	// file.getOriginalFilename();
	// } catch (Exception e) {
	// status = status + "Failed to upload " + file.getOriginalFilename()+ " " +
	// e.getMessage();
	// }
	// }
	// return status;
	// }

}
