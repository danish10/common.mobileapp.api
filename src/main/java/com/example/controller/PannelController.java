package com.example.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.bean.ApiBean;
import com.example.bean.ErrorResponse;
import com.example.entity.AppQuery;
import com.example.entity.BlackListEntity;
import com.example.entity.EventEntity;
import com.example.entity.Paramount;
import com.example.entity.ParamountServiceDetail;
import com.example.entity.ParamountServiceDetailFinal;
import com.example.entity.PostingEntity;
import com.example.entity.UnitCurrent;
import com.example.entity.UnitHistory;
import com.example.entity.UserEntity;
import com.example.entity.UserLogin;
import com.example.entity.UserLoginDetails;
import com.example.service.IGenericService;
import com.example.service.ParamountService;
import com.example.util.AES;

@RestController
@CrossOrigin
@RequestMapping("/pannel")
public class PannelController {

	private static final Logger logger = LoggerFactory.getLogger(PannelController.class);

	@Autowired
	private IGenericService<AppQuery> iAppQueryService;

	@Autowired
	private IGenericService<ApiBean> iDataGenericService;

	@Autowired
	private IGenericService<UserLogin> iUserLoginService;

	@Autowired
	private IGenericService<UserLoginDetails> iUserLoginDetailsService;

	@Autowired
	private IGenericService<UserEntity> iUserEntityService;

	@Autowired
	private IGenericService<BlackListEntity> iBlackListEntityService;

	@Autowired
	private IGenericService<EventEntity> iEventEntityService;

	@Autowired
	private IGenericService<PostingEntity> iPostingEntityService;

	@Autowired
	private IGenericService<Paramount> iParamountService;

	@Autowired
	private IGenericService<UnitCurrent> iUnitCurrentService;

	@Autowired
	private IGenericService<UnitHistory> iUnitHistoryService;

	@Autowired
	private IGenericService<ParamountServiceDetailFinal> psdfService;

	@Autowired
	private IGenericService<ParamountServiceDetail> iparamountServiceDetails;

	@Autowired
	private ParamountService parservice;

	String query = null;
	String message = "";
	String downloadApk = null;
	HashSet<String> filter = new HashSet<String>();

	HashMap<String, String> status = new HashMap<String, String>();

	@GetMapping(value = "{/find/up}")
	public ResponseEntity<?> getUsernamePassword() {

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping(value = "/get/paramount/details")
	public ResponseEntity<?> getParamountDetails(@RequestParam(value = "gsno", required = true) String gsno)
			throws ParseException {

		String current_date = new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString();

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		/* for paramout */
		Date fromDate2, toDate2;
		String sus_no2, from_date2, to_date2 = null, gsno2, unit_desc2, project2, area2, subarea2;

		/* for paramountservicedetail */
		Date fromDate1, toDate1;
		String sus_no1, from_date1, to_date1 = null, gsno1, unit_desc1, project1, area1, subarea1;

		String DateofCharge = null;
		String DateofRelieve = null;

		int TotalYear = 0;
		int TotalMonth = 0;

		List<Paramount> listParamount = iParamountService.fetch(new Paramount(), "where gsno='" + gsno + "'");
		
		List<ParamountServiceDetail> parmountdetails = parservice.paramountDetails(listParamount);
		//logger.info("{}" ,parmountdetails);
		
		List<ParamountServiceDetailFinal> checkFinalParamount = psdfService.fetch(new ParamountServiceDetailFinal(),
				"where gsno='" + gsno + "'");
		
		logger.info("Check 1 {} " , checkFinalParamount.size());

		for (ParamountServiceDetailFinal paramountServiceDetailFinal : checkFinalParamount) {
			psdfService.delete(paramountServiceDetailFinal);
		}

		
		logger.info("Check 2 {} " , checkFinalParamount.size());
		if (checkFinalParamount.size() == 0) {
			for (Paramount paramount : listParamount) {

				for (ParamountServiceDetail paramountServiceDetail : parmountdetails) {

					// System.out.println("Paramout " + paramount.getSus_no() + " And Paramount
					// service Details " + paramountServiceDetail.getSus_no());

					if (paramount.getSus_no() == paramountServiceDetail.getSus_no()) {
						/*
						 * System.out.println("############ParamoutSusno " + paramount.getSus_no() +
						 * " Equal To Paramount Service Details " + paramountServiceDetail.getSus_no());
						 * System.out.println(paramount + " " + paramountServiceDetail);
						 */

						to_date1 = paramountServiceDetail.getTo_date();

						if (to_date1 == "\r" || to_date1.equals("\r")) {

							to_date1 = current_date;
						}
						/* System.out.println("todate1 " + to_date1); */
						fromDate1 = simpleDateFormat.parse(paramountServiceDetail.getFrom_date());
						toDate1 = simpleDateFormat.parse(to_date1);

						fromDate2 = simpleDateFormat.parse(paramount.getFrom_date());
						to_date2 = paramount.getTo_date();
						if (to_date2 == "\r" || to_date2.equals("\r")) {
							to_date2 = current_date;
						}
						/* System.out.println("todate2 " + to_date2); */
						toDate2 = simpleDateFormat.parse(to_date2);

						/* fromDate2 is after from fromDate1 */
						if (fromDate2.compareTo(fromDate1) > 0) {
							/*
							 * System.out.println("********FromDate2 " + simpleDateFormat.format(fromDate2)
							 * + " FromDate1 " + simpleDateFormat.format(fromDate1));
							 */
							DateofCharge = simpleDateFormat.format(fromDate2);

						}

						else {
							/*
							 * System.out.println("Else fromdate"); System.out.println("********FromDate2 "
							 * + simpleDateFormat.format(fromDate2) + " FromDate1 " +
							 * simpleDateFormat.format(fromDate1));
							 */
							DateofCharge = simpleDateFormat.format(fromDate1);
						}

						/* toDate2 is before from toDate1 */
						if (toDate2.compareTo(toDate1) < 0) {
							/*
							 * System.out.println("********ToDate2 " + simpleDateFormat.format(toDate2) +
							 * " ToDate1 " + simpleDateFormat.format(toDate1));
							 */
							DateofRelieve = simpleDateFormat.format(toDate2);
						} else {
							/*
							 * System.out.println("Else todate"); System.out.println("********ToDate2 " +
							 * simpleDateFormat.format(toDate2) + " ToDate1 " +
							 * simpleDateFormat.format(toDate1));
							 */
							DateofRelieve = simpleDateFormat.format(toDate1);

						}
						
						String DOC=null;
						String DOR=null;
						if (DateofCharge.compareTo(DateofRelieve)>0) {
							DOC = DateofRelieve;
							DOR = DateofCharge;
						} else {
							DOC = DateofCharge;
							DOR = DateofRelieve;
						}

						
						LocalDate doc, dor;
						doc = LocalDate.parse(DOC);
					/*	System.out.println("Docccc " + doc);*/
						dor = LocalDate.parse(DOR);
						/*System.out.println("Dorrr " + dor);*/
						
						Period p = Period.between(doc, dor);

						
						
						ParamountServiceDetailFinal paramountFinal = new ParamountServiceDetailFinal();
						paramountFinal.setDoCharge(DOC);
						paramountFinal.setDoRelieve(DOR);
						paramountFinal.setUnit(paramountServiceDetail.getUnit_desc());
						paramountFinal.setGsno(paramountServiceDetail.getGsno());
						paramountFinal.setProject(paramountServiceDetail.getProject());
						paramountFinal.setYear(p.getYears());
						paramountFinal.setMonth(p.getMonths());
						paramountFinal.setDays(p.getDays());
						paramountFinal.setArea(paramountServiceDetail.getArea());
						paramountFinal.setSector(paramountServiceDetail.getSector());
						paramountFinal.setAreaEx(paramountServiceDetail.getSubarea());
						psdfService.update(paramountFinal);

					}

				}

			}

		} else {
			for (Paramount paramount : listParamount) {

				for (ParamountServiceDetail paramountServiceDetail : parmountdetails) {

					// System.out.println("Paramout " + paramount.getSus_no() + " And Paramount
					// service Details " + paramountServiceDetail.getSus_no());

					if (paramount.getSus_no() == paramountServiceDetail.getSus_no()) {
						/*
						 * System.out.println("############ParamoutSusno " + paramount.getSus_no() +
						 * " Equal To Paramount Service Details " + paramountServiceDetail.getSus_no());
						 * System.out.println(paramount + " " + paramountServiceDetail);
						 */

						to_date1 = paramountServiceDetail.getTo_date();

						if (to_date1 == "\r" || to_date1.equals("\r")) {

							to_date1 = current_date;
						}
						/* System.out.println("todate1 " + to_date1); */
						fromDate1 = simpleDateFormat.parse(paramountServiceDetail.getFrom_date());
						toDate1 = simpleDateFormat.parse(to_date1);

						fromDate2 = simpleDateFormat.parse(paramount.getFrom_date());
						to_date2 = paramount.getTo_date();
						if (to_date2 == "\r" || to_date2.equals("\r")) {
							to_date2 = current_date;
						}
						/* System.out.println("todate2 " + to_date2); */
						toDate2 = simpleDateFormat.parse(to_date2);

						/* fromDate2 is after from fromDate1 */
						if (fromDate2.compareTo(fromDate1) > 0) {
							/*
							 * System.out.println("********FromDate2 " + simpleDateFormat.format(fromDate2)
							 * + " FromDate1 " + simpleDateFormat.format(fromDate1));
							 */
							DateofCharge = simpleDateFormat.format(fromDate2);

						}

						else {
							/*
							 * System.out.println("Else fromdate"); System.out.println("********FromDate2 "
							 * + simpleDateFormat.format(fromDate2) + " FromDate1 " +
							 * simpleDateFormat.format(fromDate1));
							 */
							DateofCharge = simpleDateFormat.format(fromDate1);
						}

						/* toDate2 is before from toDate1 */
						if (toDate2.compareTo(toDate1) < 0) {
							/*
							 * System.out.println("********ToDate2 " + simpleDateFormat.format(toDate2) +
							 * " ToDate1 " + simpleDateFormat.format(toDate1));
							 */
							DateofRelieve = simpleDateFormat.format(toDate2);
						} else {
							/*
							 * System.out.println("Else todate"); System.out.println("********ToDate2 " +
							 * simpleDateFormat.format(toDate2) + " ToDate1 " +
							 * simpleDateFormat.format(toDate1));
							 */
							DateofRelieve = simpleDateFormat.format(toDate1);
						}

						String DOC=null;
						String DOR=null;
						if (DateofCharge.compareTo(DateofRelieve)>0) {
							DOC = DateofRelieve;
							DOR = DateofCharge;
						} else {
							DOC = DateofCharge;
							DOR = DateofRelieve;
						}

						
						LocalDate doc, dor;
						doc = LocalDate.parse(DOC);
					/*	System.out.println("Docccc " + doc);*/
						dor = LocalDate.parse(DOR);
						/*System.out.println("Dorrr " + dor);*/
						
						Period p = Period.between(doc, dor);

						
						
						ParamountServiceDetailFinal paramountFinal = new ParamountServiceDetailFinal();
						paramountFinal.setDoCharge(DOC);
						paramountFinal.setDoRelieve(DOR);
						paramountFinal.setUnit(paramountServiceDetail.getUnit_desc());
						paramountFinal.setGsno(paramountServiceDetail.getGsno());
						paramountFinal.setProject(paramountServiceDetail.getProject());
						paramountFinal.setYear(p.getYears());
						paramountFinal.setMonth(p.getMonths());
						paramountFinal.setDays(p.getDays());
						paramountFinal.setArea(paramountServiceDetail.getArea());
						paramountFinal.setSector(paramountServiceDetail.getSector());
						paramountFinal.setAreaEx(paramountServiceDetail.getSubarea());
						psdfService.update(paramountFinal);
						
					/*	LocalDate doc, dor;
						doc = LocalDate.parse(DateofCharge);
					
						dor = LocalDate.parse(DateofRelieve);
					
						
						Period p = Period.between(doc, dor);

						
						
						ParamountServiceDetailFinal paramountFinal = new ParamountServiceDetailFinal();
						paramountFinal.setDoCharge(DateofCharge);
						paramountFinal.setDoRelieve(DateofRelieve);
						paramountFinal.setUnit(paramountServiceDetail.getUnit_desc());
						paramountFinal.setGsno(paramountServiceDetail.getGsno());
						paramountFinal.setProject(paramountServiceDetail.getProject());
						paramountFinal.setYear(p.getYears());
						paramountFinal.setMonth(p.getMonths());
						paramountFinal.setDays(p.getDays());
						paramountFinal.setArea(paramountServiceDetail.getArea());
						paramountFinal.setSector(paramountServiceDetail.getSector());
						paramountFinal.setAreaEx(paramountServiceDetail.getSubarea());
						psdfService.update(paramountFinal);
*/
					}

				}

			}

		}

		List<ParamountServiceDetailFinal> finalList = psdfService.fetch(new ParamountServiceDetailFinal(), "where gsno='"+gsno+"'  ORDER BY do_relieve");
		return new ResponseEntity<>(finalList,HttpStatus.OK);

	}

	/****************************************************
	 * User Pannel Part
	 ****************************************************/

	@GetMapping(value = "/get/apk/status")
	public ResponseEntity<?> getApkStatus(@RequestParam(value = "gsno", required = true) String gsno) {

		boolean isAvailable = iUserLoginDetailsService.exists(new UserLoginDetails(), "Where gsno='" + gsno + "'");
		if (isAvailable) {
			UserLoginDetails uuu = iUserLoginDetailsService.find(new UserLoginDetails(), "Where gsno='" + gsno + "'");
			downloadApk = uuu.getDownload_apk();
			// System.out.println("HOW MANY DOWNLOAD APK By gsno :" + gsno + " = " +
			// downloadApk);
			return new ResponseEntity<>(downloadApk, HttpStatus.OK);
		}

		else {
			downloadApk = "0";
			// System.out.println("IT IS USER TO DOWNLOADING APK FIRST TIME");
			return new ResponseEntity<>(downloadApk, HttpStatus.OK);
		}

	}

	
	@RequestMapping(value = "/user/login/detail", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<?> userDownloadApkDetails(@RequestBody UserLoginDetails objUserLoginDetails) {

		boolean isAvailable = iUserLoginDetailsService.exists(new UserLoginDetails(),
				"Where gsno='" + objUserLoginDetails.getGsno() + "'");

		if (isAvailable) {
			query = "Where gsno='" + objUserLoginDetails.getGsno() + "'";
			UserLoginDetails uld = iUserLoginDetailsService.find(new UserLoginDetails(), query);
			uld.setDate_time(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(new Date()).toString());
			uld.setDownload_apk(objUserLoginDetails.getDownload_apk());
			logger.info("*****Update***** " + objUserLoginDetails.getGsno());
			iUserLoginDetailsService.update(uld);
		} else {
			System.out.println(objUserLoginDetails.getDownload_apk());
			objUserLoginDetails
					.setDate_time(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(new Date()).toString());
			logger.info("*******Save***** " + objUserLoginDetails.getGsno());
			iUserLoginDetailsService.save(objUserLoginDetails);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(value = "/querydata/ulogin/{beanName}")
	public ResponseEntity<?> uLogin(@PathVariable(value = "beanName") String beanName,
			@RequestParam(value = "queryid", required = true) String queryid,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "gsno", required = true) String gsno,
			@RequestParam(value = "dob", required = true) String dob) {

		logger.info("Requested Bean :" + beanName + ", Requested Parameters : {} , {} ", queryid, title);

		boolean isblacklist = iBlackListEntityService.exists(new BlackListEntity(), "Where gsno= '" + gsno + "'");
		if (isblacklist) {
			System.out.println("BlackList User");
			return new ResponseEntity<>(status, HttpStatus.LOCKED);
		} else {
			AppQuery objAppQuery = getAppQuery(queryid, title);
			status.put("status", "fails");
			if (objAppQuery == null) {
				logger.info("No query found against id : " + queryid);
				return new ResponseEntity<>(status, HttpStatus.NO_CONTENT);
			}
			if (beanName.equalsIgnoreCase("api")) {
				List<ApiBean> listData = checkLogin1(objAppQuery, gsno, dob);
				System.out.println(" **** User Details ****" + listData.toString());
				if (listData.size() == 0) {
					logger.info("No query found against query : " + objAppQuery.getQuery());
					return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
				} else {
					status.put("status", "success");
					return new ResponseEntity<>(listData, HttpStatus.OK);
				}

			}
			logger.info("Invalid fetch bean name : " + beanName);
			return new ResponseEntity<>(status, HttpStatus.NO_CONTENT);
		}

	}

	/****************************************************
	 * Admin Pannel Part *
	 ****************************************************/

	@GetMapping(value = "/get/posting/details")
	public ResponseEntity<?> getPostingsDetails() {
		
		List<PostingEntity> postingDetailsList = iPostingEntityService.fetch(new PostingEntity());
		logger.info("Total " + postingDetailsList.size() + "  Found");
		return new ResponseEntity<>(postingDetailsList, HttpStatus.OK);
	}

	@PostMapping(value = "/create/posting/details")
	public ResponseEntity<?> addPostingDetails(@RequestBody PostingEntity objPostingEntity) throws Exception {

		if (objPostingEntity.equals("") && objPostingEntity == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			boolean isEmplyeeExist = iPostingEntityService.exists(new PostingEntity(),
					"where gsno='" + objPostingEntity.getGsno() + "'");
			if (isEmplyeeExist) {
				message = "Gsno " + objPostingEntity.getGsno() + " Already Exist";
				return new ResponseEntity<>(new ErrorResponse(HttpStatus.ALREADY_REPORTED, "1", message),
						HttpStatus.OK);
			} else {
				iPostingEntityService.save(objPostingEntity);
				logger.info("GsNo " + objPostingEntity.getGsno() + " Add Successfully");
				return new ResponseEntity<>(HttpStatus.OK);
			}
		}

	}

	@RequestMapping(value = "/delete/posting/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deletePosting(@PathVariable("id") String id) {

		PostingEntity objPostingEntity = iPostingEntityService.find(new PostingEntity(), "Where id='" + id + "'");
		if (objPostingEntity != null && !objPostingEntity.equals("")) {
			iPostingEntityService.delete(objPostingEntity);
			logger.info("gsno " + objPostingEntity.getGsno() + " Deleted Successfully");
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			logger.info("gsno " + objPostingEntity.getGsno() + " Not Deleted");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	@PutMapping(value = "/update/posting/details")
	// @CacheEvict(cacheNames = "objAddNewsEntity")
	public ResponseEntity<?> updatePosting(@RequestBody PostingEntity objPostingEntity) {

		if (objPostingEntity != null && !objPostingEntity.equals("")) {
			logger.info("Gsno " + objPostingEntity.getGsno() + "Updated Successfully");
			iPostingEntityService.update(objPostingEntity);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			logger.info(" Error In Update gsno " + objPostingEntity.getGsno() + " Update");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	@PostMapping(value = "/add/user/manually/blacklist")
	public ResponseEntity<?> saveUserManuallyBlackList(@RequestBody ApiBean objApiBean) throws Exception {
		logger.info(objApiBean.toString());
		System.out.println(objApiBean.getGsno());
		System.out.println(objApiBean.getMobile());
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(value = "/add/user/blacklist")
	public ResponseEntity<?> saveUser(@RequestBody List<ApiBean> objApiBean) throws Exception {
		logger.info(objApiBean.toString());
		logger.info("BlackList User size: " + objApiBean.size());
		for (ApiBean apiBean : objApiBean) {
			// System.out.println(apiBean.getGsno());
			// System.out.println(apiBean.getDob());
			// System.out.println(apiBean.getName());
			// System.out.println(apiBean.getMobile());

			BlackListEntity ble = new BlackListEntity();
			ble.setId(apiBean.getId());
			ble.setGsno(apiBean.getGsno());
			ble.setDob(apiBean.getDob());
			ble.setMobile(apiBean.getMobile());
			ble.setName(apiBean.getName());
			ble.setDatetime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(new Date()).toString());
			iBlackListEntityService.update(ble);
			logger.info("Add " + objApiBean.size() + " User inBlackList Succesfully");
			logger.info("Add For Id " + apiBean.getId() + " And Gs No " + apiBean.getGsno());

		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(value = "/delete/user/blacklist")
	public ResponseEntity<?> delteBlackListUser(@RequestBody List<BlackListEntity> objApiBean) throws Exception {

		logger.info(objApiBean.toString());
		logger.info("BlackList User size: " + objApiBean.size());
		for (BlackListEntity apiBean : objApiBean) {
			// System.out.println("Data......." + apiBean.toString());
			BlackListEntity ble = iBlackListEntityService.find(new BlackListEntity(),
					"Where id='" + apiBean.getId() + "'");
			System.out.println("Delete.......For Id " + ble.getId() + " And  Gs No " + ble.getGsno());
			iBlackListEntityService.delete(ble);
			System.out.println("Delete Successfully.......");
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(value = "/create/account/admin")
	public ResponseEntity<?> saveUser(@RequestBody UserEntity objUser) throws Exception {

		if (objUser != null) {
			objUser.getPassword();
			objUser.setPassword(AES.encrypt(objUser.getPassword()));
			// objUser.setPassword(PasswordEncription.generateHash(objUser.getPassword()));
			objUser.setCreateDate(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(new Date()).toString());

			iUserEntityService.save(objUser);
			logger.info("####User Create Succesfully####" + objUser);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} else {

			logger.info("####Errro Create User####");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	@PostMapping(value = "/login/admin")
	public ResponseEntity<?> doLogin(@RequestBody UserEntity objUser) throws Exception {

		logger.info("####Do Login#### loging id and Password {} {}"  ,objUser.getLoginId(),objUser.getPassword());

		objUser.setPassword(AES.encrypt(objUser.getPassword()));
		String query = "where loginId ='" + objUser.getLoginId() + "' and password ='" + objUser.getPassword()
				+ "' and flag  = false";

		UserEntity dentity = iUserEntityService.find(new UserEntity(), query);
		
		logger.info("Do Login By : {}" ,  dentity);
		if (dentity != null) {
			return new ResponseEntity<>(dentity, HttpStatus.OK);
		}

		logger.info("Error in Login");
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@GetMapping(value = "/get/downlaod/apk/details")
	public ResponseEntity<?> getDownloadApkDetails() {

		List<UserLoginDetails> userLoginDetailsList = iUserLoginDetailsService.fetch(new UserLoginDetails());
		return new ResponseEntity<>(userLoginDetailsList, HttpStatus.OK);
	}

	@GetMapping(value = "/get/blacklist/users/details")
	public ResponseEntity<?> getBlackListUserDetails() {
		List<BlackListEntity> blackList = iBlackListEntityService.fetch(new BlackListEntity());
		logger.info("Total " + blackList.size() + " Black List User Found");
		return new ResponseEntity<>(blackList, HttpStatus.OK);
	}

	// @GetMapping(value = "/querydata/getUserDetails/{beanName}")
	// public ResponseEntity<?> getUserDetails(@PathVariable(value = "beanName")
	// String beanName,
	// @RequestParam(value = "queryid", required = true) String queryid,
	// @RequestParam(value = "title", required = false) String title) {
	//
	//
	// logger.info("Requested Bean :" + beanName + ", Requested Parameters : {} , {}
	// ", queryid, title);
	// AppQuery objAppQuery = getAppQuery(queryid, title);
	// status.put("status", "fails");
	// if (objAppQuery == null) {
	// logger.info("No query found against id : " + queryid);
	// return new ResponseEntity<>(status, HttpStatus.NO_CONTENT);
	// }
	//
	// if (beanName.equalsIgnoreCase("api")) {
	// List<ApiBean> listData = fetchData(objAppQuery);
	//
	//
	//
	//
	// System.out.println(" ****Find " + listData.size() + " User Details****");
	// if (listData.size() == 0) {
	// logger.info("No query found against query : " + objAppQuery.getQuery());
	// return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
	// } else {
	// status.put("status", "success");
	// return new ResponseEntity<>(listData, HttpStatus.OK);
	// }
	// }
	//
	// logger.info("Invalid fetch bean name : " + beanName);
	// return new ResponseEntity<>(status, HttpStatus.NO_CONTENT);
	//
	// }

	@GetMapping(value = "/querydata/getUserDetails/{beanName}")
	public ResponseEntity<?> getUserDetails(@PathVariable(value = "beanName") String beanName,
			@RequestParam(value = "queryid", required = true) String queryid,
			@RequestParam(value = "title", required = false) String title) {

		logger.info("Requested Bean :" + beanName + ", Requested Parameters : {} , {} ", queryid, title);
		AppQuery objAppQuery = getAppQuery(queryid, title);
		status.put("status", "fails");
		if (objAppQuery == null) {
			logger.info("No query found against id : " + queryid);
			return new ResponseEntity<>(status, HttpStatus.NO_CONTENT);
		}

		if (beanName.equalsIgnoreCase("api")) {
			List<ApiBean> listData = fetchData(objAppQuery);

			List<Integer> indexList = new ArrayList<Integer>();

			System.out.println(" ****Find " + listData.size() + " User Details****");
			System.out.println(" ****Find " + listData + " User Details****");

			List<BlackListEntity> blackList = iBlackListEntityService.fetch(new BlackListEntity());

			System.out.println("**blackListuser size : " + blackList.size() + "**");
			System.out.println("**blackListuser list: " + blackList + "**");

			for (int i = 0; i < listData.size(); i++) {

				boolean found = false;
				for (BlackListEntity str2 : blackList) {
					ApiBean str1 = listData.get(i);
					if (str1.getGsno().equals(str2.getGsno())) {
						// found = true;
						// indexList.add(i);
						// System.out.println(" First " +str1.getGsno());
						// System.out.println(" Second " +str2.getGsno());
						listData.remove(i);
						i--;
						break;
					}

				}

			}

			System.out.println(" ****Find " + listData.size() + " User Details****");
			System.out.println(" ****Find " + listData.toString() + " User Details****");

			if (listData.size() == 0) {
				logger.info("No query found against query : " + objAppQuery.getQuery());
				return new ResponseEntity<>(status, HttpStatus.BAD_REQUEST);
			} else {
				status.put("status", "success");
				return new ResponseEntity<>(listData, HttpStatus.OK);
			}
		}

		logger.info("Invalid fetch bean name : " + beanName);
		return new ResponseEntity<>(status, HttpStatus.NO_CONTENT);

	}

	/****************************************************
	 * Commman Method Pannel Part
	 ****************************************************/

	private List<ApiBean> fetchData(AppQuery objAppQuery) {
		List<ApiBean> list = null;
		String query = "";
		query = objAppQuery.getQuery();
		logger.info("query : " + query);
		list = iDataGenericService.nativeQuery(new ApiBean(), query);
		logger.info("API List Size : " + list.size());
		return list;
	}

	private AppQuery getAppQuery(String id, String title) {
		AppQuery objAppQuery = null;
		if (id != null && title != null) {
			objAppQuery = iAppQueryService.find(new AppQuery(), " where id = " + id + " and title = '" + title + "'");
		} else if (id != null) {
			objAppQuery = iAppQueryService.find(new AppQuery(), " where id = " + id);
		} else if (title != null) {
			objAppQuery = iAppQueryService.find(new AppQuery(), " where title = '" + title + "'");
		}
		return objAppQuery;
	}

	private List<ApiBean> checkLogin1(AppQuery objAppQuery, String gsno, String dob) {
		List<ApiBean> list = null;
		String query = null;
		if (gsno != null && dob != null) {
			query = objAppQuery.getQuery().replace("<gsno>", gsno);
			query = query.replace("<dob>", dob);
		} else {
			query = objAppQuery.getQuery();
		}
		logger.info("query : " + query);
		list = iDataGenericService.nativeQuery(new ApiBean(), query);
		logger.info("API List Size : " + list.size());
		return list;
	}

}
