package com.example.controller;

import com.example.bean.ApiBean;
import com.example.bean.CommonBean;
import com.example.bean.Test;
import com.example.entity.AppQuery;
import com.example.entity.Paramount;
import com.example.service.IGenericService;
import com.example.service.ParamountService;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping({"/querydata"})
public class QueryResultController
{
  private static final Logger logger = LoggerFactory.getLogger(QueryResultController.class);
  @Autowired
  private IGenericService<AppQuery> iAppQueryService;
  @Autowired
  private IGenericService<Test> iTestGenericService;
  @Autowired
  private IGenericService<ApiBean> iDataGenericService;
  @Autowired
  private IGenericService<CommonBean> iCommonGenericService;
  
  @Autowired
	private IGenericService<Paramount> iParamountService;
  @Autowired
	private ParamountService parservice;
  
  String query = "";
  
  @GetMapping({"/{beanName}"})
  public ResponseEntity<?> findByQueryTest1(@PathVariable("beanName") String beanName,
		  @RequestParam(value="id", required=false) String id,
		  @RequestParam(value="title", required=false) String title, 
		  @RequestParam(value="gsno", required=false) String gsno, 
		  @RequestParam(value="susno", required=false) String susno)
  {
    logger.info("Requested Bean :" + beanName + ", Requested Parameters : {} , {} ", id, title);
    
    
    
    if(id.equals("10")) {
    	
    	List<Paramount> listParamount = iParamountService.fetch(new Paramount(), "where gsno='" + gsno + "'");
    	logger.info("Paramout Details-------------------------- : " + listParamount);
    	//List<Paramount> adc = parservice.paramountDetails(listParamount);
    	logger.info("Paramout Details-------------------------- : " );
    	//return new ResponseEntity(listCommon, HttpStatus.OK);
    	return new ResponseEntity<>(HttpStatus.OK);
    }else {
    	AppQuery objAppQuery = getAppQuery(id, title);
        if (objAppQuery == null)
        {
          logger.info("No query found against id : " + id);
          return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        if (beanName.equalsIgnoreCase("Test"))
        {
          List<Test> listTest = fetchTest(objAppQuery);
          if (listTest == null)
          {
            logger.info("No query found against query : " + objAppQuery.getQuery());
            return new ResponseEntity(HttpStatus.NO_CONTENT);
          }
          return new ResponseEntity(listTest, HttpStatus.OK);
        }
        if (beanName.equalsIgnoreCase("api"))
        {
          List<ApiBean> listData = fetchData(objAppQuery, gsno, susno);
          if (listData == null)
          {
            logger.info("No query found against query : " + objAppQuery.getQuery());
            return new ResponseEntity(HttpStatus.NO_CONTENT);
          }
          return new ResponseEntity(listData, HttpStatus.OK);
        }
        if (beanName.equalsIgnoreCase("gen"))
        {
          List<CommonBean> listCommon = fetchCommonData(objAppQuery, gsno, susno);
          if (listCommon == null)
          {
            logger.info("No query found against query : " + objAppQuery.getQuery());
            return new ResponseEntity(HttpStatus.NO_CONTENT);
          }
          
      
          return new ResponseEntity(listCommon, HttpStatus.OK);
    }
     
    }
    logger.info("Invalid fetch bean name : " + beanName);
    return new ResponseEntity(HttpStatus.NO_CONTENT);
  }
  
  private AppQuery getAppQuery(String id, String title)
  {
    AppQuery objAppQuery = null;
    if ((id != null) && (title != null)) {
      objAppQuery = (AppQuery)this.iAppQueryService.find(new AppQuery(), " where id = " + id + " and title = '" + title + "'");
    } else if (id != null) {
      objAppQuery = (AppQuery)this.iAppQueryService.find(new AppQuery(), " where id = " + id);
    } else if (title != null) {
      objAppQuery = (AppQuery)this.iAppQueryService.find(new AppQuery(), " where title = '" + title + "'");
    }
    return objAppQuery;
  }
  
  private List<Test> fetchTest(AppQuery objAppQuery)
  {
    List<Test> list = null;
    list = this.iTestGenericService.nativeQuery(new Test(), objAppQuery.getQuery());
    logger.info("Test List Size : " + list.size());
    return list;
  }
  
  private List<ApiBean> fetchData(AppQuery objAppQuery, String gsno, String susno)
  {
    List<ApiBean> list = null;
    String query = "";
    if ((gsno != null) && (susno != null))
    {
      query = objAppQuery.getQuery().replace("<gsno>", gsno);
      String temp = query.replace("<susno>", susno);
      query = "";
      query = temp;
    }
    else if ((gsno != null) && (susno == null))
    {
      query = objAppQuery.getQuery().replace("<gsno>", gsno);
    }
    else if ((gsno == null) && (susno != null))
    {
      query = objAppQuery.getQuery().replace("<susno>", susno);
    }
    else
    {
      query = objAppQuery.getQuery();
    }
    logger.info("query : " + query);
    list = this.iDataGenericService.nativeQuery(new ApiBean(), query);
    logger.info("API List Size : " + list.size());
    return list;
  }
  
  private List<CommonBean> fetchCommonData(AppQuery objAppQuery, String gsno, String susno)
  {
    List<CommonBean> list = null;
    String query = "";
    if ((gsno != null) && (susno != null))
    {
      query = objAppQuery.getQuery().replace("<gsno>", gsno);
      String temp = query.replace("<susno>", susno);
      query = "";
      query = temp;
    }
    else if ((gsno != null) && (susno == null))
    {
      query = objAppQuery.getQuery().replace("<gsno>", gsno);
    }
    else if ((gsno == null) && (susno != null))
    {
      query = objAppQuery.getQuery().replace("<susno>", susno);
    }
    else
    {
      query = objAppQuery.getQuery();
    }
    logger.info("query : " + query);
    list = this.iCommonGenericService.nativeQuery(new CommonBean(), query);
    logger.info("Data List Size : " + list.size());
    return list;
  }
}
