package com.example.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.Test1;
import com.example.service.IGenericService;

@RestController
@CrossOrigin
@RequestMapping(value = "/test1")
public class Test1Controller {

	private static final Logger logger = LoggerFactory.getLogger(Test1Controller.class);

	@Autowired
	private IGenericService<Test1> iTest1Service;

	@PostMapping()
	public ResponseEntity<?> saveTest1(@RequestBody Test1 objTest1) {
		iTest1Service.save(objTest1);
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PutMapping()
	public ResponseEntity<?> updateTest1(@RequestBody Test1 objTest1) {
		iTest1Service.update(objTest1);
		logger.info("Data update successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@DeleteMapping()
	public ResponseEntity<?> deleteTest1(@RequestParam(value = "id", required = true) String id) {
		Test1 objTest1 = iTest1Service.find(new Test1(), "where id = " + id);
		if (objTest1 == null) {
			return new ResponseEntity<>(objTest1, HttpStatus.NO_CONTENT);
		}
		iTest1Service.delete(objTest1);
		logger.info("Data deleted successfully !");
		return new ResponseEntity<>(HttpStatus.GONE);
	}

	@GetMapping()
	public ResponseEntity<?> findTest1(@RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "text1", required = false) String text1) {

		if (id == null && text1 == null) {
			return new ResponseEntity<>(iTest1Service.fetch(new Test1()), HttpStatus.OK);
		}

		Test1 objTest1 = null;
		if (id != null && text1 == null) {
			objTest1 = iTest1Service.find(new Test1(), "where id = " + id);
		} else if (id == null && text1 != null) {
			objTest1 = iTest1Service.find(new Test1(), " where text1 = '" + text1 + "'");
		} else if (id != null && text1 != null) {
			objTest1 = iTest1Service.find(new Test1(), " where id = " + id + " and text1 = '" + text1 + "'");
		}

		if (objTest1 == null) {
			return new ResponseEntity<>(objTest1, HttpStatus.NO_CONTENT);
		}

		logger.info("Data found! ");
		return new ResponseEntity<>(objTest1, HttpStatus.OK);
	}
}
