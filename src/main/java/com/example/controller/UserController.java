package com.example.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections4.ListUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.bean.ApiBean;
import com.example.bean.ErrorResponse;
import com.example.bean.Grievance;
import com.example.bean.UserLoginBean;
import com.example.entity.AppQuery;
import com.example.entity.Paramount;
import com.example.entity.UnitCurrent;
import com.example.entity.UnitHistory;
import com.example.entity.UserLogin;
import com.example.service.IGenericService;

@RestController
@CrossOrigin
public class UserController {
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private IGenericService<AppQuery> iAppQueryService;
	@Autowired
	private IGenericService<Grievance> iGrievanceService;

	@Autowired
	private IGenericService<ApiBean> iDataGenericService;
	@Autowired
	private IGenericService<UserLogin> iUserLoginService;

	@Autowired
	private IGenericService<Paramount> iParamountService;

	@Autowired
	private IGenericService<UnitCurrent> iUnitCurrentService;

	@Autowired
	private IGenericService<UnitHistory> iUnitHistoryService;

	String query = null;
	String message = "";
	@Value("${filepath.profile}")
	private String UPLOADED_PROFILE;

	@Value("${filepath.jointphoto}")
	private String UPLOADED_JOINTPHOTO;

	HashMap<String, String> status = new HashMap();

	String current_raiseDate;
	String current_closeDate;
	String history_raiseDate;
	String history_closeDate;
	String current_date = "";

	@PostMapping({ "/querydata/paramout" })
	public ResponseEntity<?> getUserParamout(@RequestParam(value = "queryid", required = true) String queryid,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "gsno", required = true) String gsno) {

		logger.info("Requested Parameters : {} ", queryid);
		AppQuery objAppQuery = getAppQuery(queryid, title);
		if (objAppQuery == null) {
			String message = "No query found against query : ";
			logger.info(message);
			List<ErrorResponse> errorResponseList = new ArrayList();
			errorResponseList.add(new ErrorResponse(HttpStatus.UNAUTHORIZED, "0", message));
			return new ResponseEntity(errorResponseList, HttpStatus.UNAUTHORIZED);
		}

		List<Paramount> listParamount = iParamountService.nativeQuery(new Paramount(),
				objAppQuery.getQuery().replace("<gsno>", gsno));
		logger.info("**********************" + listParamount);
		logger.info("Paramout List Size : " + listParamount.size() + " found for @ :" + gsno);
		if (listParamount.size() == 0) {
			String message = "No query foung against query : " + objAppQuery.getQuery();
			logger.info(message);
			List<ErrorResponse> errorResponseList = new ArrayList();
			errorResponseList.add(new ErrorResponse(HttpStatus.BAD_REQUEST, "0", message));
			return new ResponseEntity(errorResponseList, HttpStatus.BAD_REQUEST);
		}

		current_date = new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString();
		System.out.println("Current Date : " + current_date);

		List<String> finalStrings = null;
		
		
		for (Paramount paramount : listParamount) {

			//System.out.println("Unit Id : " + paramount.getSus_no());
			//System.out.println("From Date : " + paramount.getFrom_date());
			//System.out.println("To Date : " + paramount.getTo_date());

		
			 String query = "where '"+paramount.getFrom_date()+"' BETWEEN raise_date AND close_date AND sus_no='"+paramount.getSus_no()+"'";
			 List<UnitHistory>listuh = iUnitHistoryService.fetch(new UnitHistory(),query);
			
			 String query1 = "where '"+paramount.getTo_date()+"' BETWEEN raise_date AND close_date AND sus_no='"+paramount.getSus_no()+"'";
			 List<UnitHistory>listuh1 = iUnitHistoryService.fetch(new UnitHistory(),query1);

//			 System.out.println("**********UnitHistory 1 " + listuh);
//			 System.out.println("**********UnitHistory 2 " + listuh1);
			 
			 List<UnitHistory> listfinal = ListUtils.union(listuh, listuh1);
			 List<String> strings = Arrays.asList(listfinal.toString());
			// System.out.println("**********UnitHistory Final List " + strings);
			 

			List<UnitCurrent> aaa = iUnitCurrentService.fetch(new UnitCurrent(),"where sus_no='" + paramount.getSus_no() + "'");
			for (UnitCurrent unitCurrent : aaa) {
				current_raiseDate = unitCurrent.getRaise_date();
			//	System.out.println("Raise Date in Unit Current : " + current_raiseDate);

				current_closeDate = unitCurrent.getClose_date();
				//System.out.println("Close Date in Unit Current : " + current_closeDate);

			}

			//System.out.println("Check Current close Date is empty or not : " + current_closeDate);
			String query3;
			if (current_closeDate.equals("\r") || current_closeDate == "\r") {
				//System.out.println("enter into empty");
				query3 = "where '" + paramount.getFrom_date() + "' BETWEEN '" + current_raiseDate + "' AND '"
						+ current_date + "' AND sus_no='" + paramount.getSus_no() + "'";
				//System.out.println("query enter into empty : " + query3);
			} else {
				//System.out.println("enter into not empty");
				query3 = "where '" + paramount.getFrom_date() + "' BETWEEN '" + current_raiseDate + "' AND '"
						+ current_closeDate + "' AND sus_no='" + paramount.getSus_no() + "'";
				//System.out.println("query enter into not empty : " + query3);

			}

			List<UnitCurrent> listuc1 = iUnitCurrentService.fetch(new UnitCurrent(), query3);
			//System.out.println("**********iUnitCurrentService 1 " + listuc1);

			
			String query4;
			if (current_closeDate.equals("\r") || current_closeDate == "\r") {
				//System.out.println("enter into empty");
				query4 = "where '" + paramount.getTo_date()+ "' BETWEEN '" + current_raiseDate + "' AND '"+ current_date + "' AND sus_no='" + paramount.getSus_no() + "'";
				//System.out.println("query enter into empty : " + query4);
			} else {
				//System.out.println("enter into not empty");
				query4 = "where '" + paramount.getTo_date() + "' BETWEEN '" + current_raiseDate + "' AND '"
						+ current_closeDate + "' AND sus_no='" + paramount.getSus_no() + "'";
				//System.out.println("query enter into not empty : " + query4);

			}

			List<UnitCurrent> listuc2 = iUnitCurrentService.fetch(new UnitCurrent(), query4);
			
			
//			System.out.println("**********UnitCurrent 1 " + listuc1);
//			System.out.println("**********UnitCurrent 2 " + listuc2);
			
			List<UnitCurrent> listfinal1 = ListUtils.union(listuc1, listuc2);
			List<String> strings1 = Arrays.asList(listfinal1.toString());
			 //System.out.println("**********UnitCurrent Final List " + strings1);
			finalStrings = ListUtils.union(strings, strings1);
			System.out.println("**********Final List*************** " + finalStrings);
			
					
		}

		return new ResponseEntity<>(finalStrings, HttpStatus.OK);

	}

	@PostMapping({ "/querydata/allgrievance" })
	public ResponseEntity<?> getUserGrievance(@RequestParam(value = "queryid", required = true) String queryid,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "gsno", required = true) String gsno) {
		logger.info("Requested Parameters : {} ", queryid);

		AppQuery objAppQuery = getAppQuery(queryid, title);
		if (objAppQuery == null) {
			String message = "No query found against query : ";
			logger.info(message);
			List<ErrorResponse> errorResponseList = new ArrayList();
			errorResponseList.add(new ErrorResponse(HttpStatus.UNAUTHORIZED, "0", message));
			return new ResponseEntity(errorResponseList, HttpStatus.UNAUTHORIZED);
		}
		List<Grievance> listGrievance = this.iGrievanceService.nativeQuery(new Grievance(),
				objAppQuery.getQuery().replace("<gsno>", gsno));
		if (listGrievance.size() == 0) {
			String message = "No query found against query : " + objAppQuery.getQuery();
			logger.info(message);
			List<ErrorResponse> errorResponseList = new ArrayList();
			errorResponseList.add(new ErrorResponse(HttpStatus.BAD_REQUEST, "0", message));
			return new ResponseEntity(errorResponseList, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity(listGrievance, HttpStatus.OK);
	}

	@PostMapping({ "/querydata/grievance" })
	public ResponseEntity<?> usergrievance(@RequestParam(value = "queryid", required = true) String queryid,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "gsno", required = true) String gsno,
			@RequestParam(value = "grievance_type", required = true) String grievance_type,
			@RequestParam(value = "grievance_message", required = true) String grievance_message) {
		logger.info("Requested Parameters : {} ", queryid);

		AppQuery objAppQuery = getAppQuery(queryid, title);
		if (objAppQuery == null) {
			String message = "No query found against query";
			logger.info(message);
			ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST, "0", message);
			return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
		}
		int i = saveGrievance(objAppQuery, gsno, grievance_type, grievance_message);
		if (i > 0) {
			this.message = "Grievance saved successfully ! ";
			return new ResponseEntity(new ErrorResponse(HttpStatus.OK, "1", this.message), HttpStatus.OK);
		}
		this.message = "Grievance not save  ! ";
		return new ResponseEntity(new ErrorResponse(HttpStatus.OK, "0", this.message), HttpStatus.OK);
	}

	@PostMapping({ "/querydata/login/{beanName}" })
	public ResponseEntity<?> userLogin(@PathVariable("beanName") String beanName,
			@RequestParam(value = "queryid", required = true) String queryid,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "userid", required = true) String userid,
			@RequestParam(value = "imei", required = true) String imei,
			@RequestParam(value = "password", required = true) String password) {
		// , @RequestParam(value="imei", required=true) String imei
		logger.info("Requested Bean :" + beanName + ", Requested Parameters : {} , {} ", queryid, title);

		AppQuery objAppQuery = getAppQuery(queryid, title);
		if (objAppQuery == null) {
			String message = "No query found against query : " + objAppQuery.getQuery();
			logger.info(message);
			List<ErrorResponse> errorResponseList = new ArrayList();
			errorResponseList.add(new ErrorResponse(HttpStatus.UNAUTHORIZED, "0", message));
			return new ResponseEntity(errorResponseList, HttpStatus.UNAUTHORIZED);
		}
		if (beanName.equalsIgnoreCase("api")) {
			List<ApiBean> listData = checkLogin(objAppQuery, userid, password, imei);
			// imei
			if (listData.size() == 0) {
				String message = "No query found against query : " + objAppQuery.getQuery();
				logger.info(message);
				List<ErrorResponse> errorResponseList = new ArrayList();
				errorResponseList.add(new ErrorResponse(HttpStatus.BAD_REQUEST, "0", message));
				return new ResponseEntity(errorResponseList, HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity(listData, HttpStatus.OK);
		}
		String message = "Invalid fetch bean name : " + beanName;
		logger.info(message);
		List<ErrorResponse> errorResponseList = new ArrayList();
		errorResponseList.add(new ErrorResponse(HttpStatus.NO_CONTENT, "0", message));
		return new ResponseEntity(errorResponseList, HttpStatus.NO_CONTENT);
	}

	@PostMapping({ "/querydata/signup/{beanName}" })
	public ResponseEntity<?> userSignUp(UserLogin userLogin, @PathVariable("beanName") String beanName,
			@RequestParam(value = "validateQueryId", required = true) String validateQueryId,
			@RequestParam(value = "signupQueryId", required = true) String signupQueryId,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "gsno", required = true) String gsno,
			@RequestParam(value = "userid", required = true) String userid,
			@RequestParam(value = "dob", required = true) String dob,
			@RequestParam(value = "mobile", required = false) String mobile,
			@RequestParam(value = "aadhar", required = false) String aadhar,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "imei", required = true) String imei,
			@RequestParam(value = "password", required = true) String password) {
		// , @RequestParam(value="imei", required=true) String imei
		logger.info("Requested Bean :" + beanName + ", Requested Parameters : {} , {} ", validateQueryId, title);

		logger.info("gsno :" + gsno);
		logger.info("userid" + userid);
		logger.info("dob :" + dob);
		logger.info("mobile :" + mobile);
		logger.info("aadhar :" + aadhar);
		logger.info("email :" + email);
		logger.info("imei :" + imei);

		AppQuery objAppQuery = getAppQuery(validateQueryId, title);
		if (objAppQuery == null) {
			String message = "No query found against id : " + validateQueryId;
			logger.info(message);
			List<ErrorResponse> errorResponseList = new ArrayList();
			errorResponseList.add(new ErrorResponse(HttpStatus.UNAUTHORIZED, "0", message));
			return new ResponseEntity(errorResponseList, HttpStatus.UNAUTHORIZED);
		}
		if (beanName.equalsIgnoreCase("api")) {
			List<ApiBean> listData = checkUser(objAppQuery, gsno, dob);
			if (listData.size() == 0) {
				String message = "No user detils found in master  : " + objAppQuery.getQuery();
				logger.info(message);
				List<ErrorResponse> errorResponseList = new ArrayList();
				errorResponseList.add(new ErrorResponse(HttpStatus.UNAUTHORIZED, "0", message));
				return new ResponseEntity(errorResponseList, HttpStatus.UNAUTHORIZED);
			}
			if (!this.iUserLoginService.exists(userLogin,
					" where userid = '" + userid + "' OR  gsno = '" + gsno + "' OR  aadhar = '" + aadhar
							+ "' OR  email = '" + email + "'  OR  mobile = '" + mobile + "' OR  imei = '" + imei
							+ "'")) {
				// OR imei = '"+imei+"'

				AppQuery saveUserQuery = getAppQuery(signupQueryId, title);
				if (saveUserQuery == null) {
					String message = "No query found against id : " + signupQueryId;
					logger.info(message);
					List<ErrorResponse> errorResponseList = new ArrayList();
					errorResponseList.add(new ErrorResponse(HttpStatus.CONFLICT, "0", message));
					return new ResponseEntity(errorResponseList, HttpStatus.CONFLICT);
				}
				// imei
				String query = userSignup(saveUserQuery, gsno, userid, dob, email, mobile, aadhar, imei, password);
				if (query != null) {
					listData = this.iDataGenericService.nativeQuery(new ApiBean(), query);
				}
				return new ResponseEntity(listData, HttpStatus.OK);
			}
			this.message = "user already exist ";
			logger.info(this.message);
			List<ErrorResponse> errorResponseList = new ArrayList();
			errorResponseList.add(new ErrorResponse(HttpStatus.CONFLICT, "0", this.message));
			return new ResponseEntity(errorResponseList, HttpStatus.CONFLICT);
		}
		this.message = ("Invalid fetch bean name : " + beanName);
		logger.info(this.message);
		List<ErrorResponse> errorResponseList = new ArrayList();
		errorResponseList.add(new ErrorResponse(HttpStatus.NO_CONTENT, "0", this.message));
		return new ResponseEntity(errorResponseList, HttpStatus.NO_CONTENT);
	}

	@PutMapping({ "/querydata/userupdate/{beanName}" })
	public ResponseEntity<?> userSignUpUpdate(UserLogin userLogin, @PathVariable("beanName") String beanName,
			@RequestParam(value = "gsno", required = true) String gsno,
			@RequestParam(value = "dob", required = false) String dob,
			@RequestParam(value = "mobile", required = false) String mobile,
			@RequestParam(value = "aadhar", required = false) String aadhar,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "password", required = false) String password,
			@RequestPart(value = "profile", required = false) MultipartFile profile, HttpServletRequest request) {
		String fileName = null;
		String fetchFile = null;
		if (beanName.equalsIgnoreCase("api")) {
			if (profile != null) {
				try {
					fileName = uploadDocuments(gsno, profile, "PROFILE");
					if (fileName != null) {
						String save = this.UPLOADED_PROFILE.substring(this.UPLOADED_PROFILE.lastIndexOf("\\") + 1,
								this.UPLOADED_PROFILE.length());
						fetchFile = request.getScheme() + "://" + request.getServerName() + ":"
								+ request.getServerPort() + request.getContextPath() + "/" + save + "/" + gsno + "/"
								+ fileName;
					}
				} catch (Exception e) {
					logger.info("error profile image save");
				}
			} else {
				logger.info("No profile image save");
			}
			this.message = "Profile update successfully ! ";
			if ((email != null) || (mobile != null) || (dob != null) || (password != null) || (aadhar != null)
					|| (fetchFile != null)) {
				int i = userSignupUpdate(gsno, email, mobile, aadhar, password, fetchFile);
				this.message = "Profile update successfully ! ";
				return new ResponseEntity(new ErrorResponse(HttpStatus.OK, "1", this.message), HttpStatus.OK);
			}
			this.message = "Profile not update  ! ";
			return new ResponseEntity(new ErrorResponse(HttpStatus.OK, "0", this.message), HttpStatus.OK);
		}
		this.message = ("Invalid fetch bean name : " + beanName);
		logger.info(this.message);
		return new ResponseEntity(new ErrorResponse(HttpStatus.NO_CONTENT, "0", this.message), HttpStatus.OK);
	}

	@PutMapping({ "/querydata/userupdate/profile" })
	public ResponseEntity<?> userSignUpUpdate(@RequestBody UserLoginBean userLoginBean, HttpServletRequest request) {
		String fileName = null;
		String fetchFile = null;
		this.message = "";
		int i;
		if ((userLoginBean.getImageBase64() != null) && (!userLoginBean.getImageBase64().trim().equals(""))) {
			try {
				fileName = uploadBase64Documents(this.UPLOADED_PROFILE, userLoginBean.getGsno(),
						userLoginBean.getImageBase64(), "PROFILE");
				if (fileName != null) {
					String save = this.UPLOADED_PROFILE.substring(this.UPLOADED_PROFILE.lastIndexOf("\\") + 1,
							this.UPLOADED_PROFILE.length());
					fetchFile = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
							+ request.getContextPath() + "/" + save + "/" + userLoginBean.getGsno() + "/" + fileName;
					this.message += "Profile Photo ";
					i = userProfileImgUpdate(fetchFile, userLoginBean.getGsno(), 1);
				}
			} catch (Exception e) {
				logger.info("error profile image save");
			}
		}

		if ((userLoginBean.getJointBase64() != null) && (!userLoginBean.getJointBase64().trim().equals(""))) {
			try {
				fileName = uploadBase64Documents(this.UPLOADED_JOINTPHOTO, userLoginBean.getGsno(),
						userLoginBean.getJointBase64(), "JOINTPHOTO");
				if (fileName != null) {
					String save = this.UPLOADED_JOINTPHOTO.substring(this.UPLOADED_JOINTPHOTO.lastIndexOf("\\") + 1,
							this.UPLOADED_JOINTPHOTO.length());
					fetchFile = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
							+ request.getContextPath() + "/" + save + "/" + userLoginBean.getGsno() + "/" + fileName;
					if (!this.message.isEmpty()) {
						this.message += ", Joint photo ";
					} else {
						this.message += " Joint photo ";
					}
					i = userProfileImgUpdate(fetchFile, userLoginBean.getGsno(), 2);
				}
			} catch (Exception e) {
				logger.info("error joint image save");
			}
		}
		if ((userLoginBean.getEmail() != null) && (!userLoginBean.getEmail().trim().equals(""))) {
			i = userProfileImgUpdate(userLoginBean.getEmail(), userLoginBean.getGsno(), 3);
			if (!this.message.isEmpty()) {
				this.message += ", Email ";
			} else {
				this.message += " Email";
			}
		}
		if ((userLoginBean.getMobile() != null) && (!userLoginBean.getMobile().trim().equals(""))) {
			i = userProfileImgUpdate(userLoginBean.getMobile(), userLoginBean.getGsno(), 4);
			if (!this.message.isEmpty()) {
				this.message += ", Mobile ";
			} else {
				this.message += " Mobile ";
			}
		}
		UserLogin updatedDetails = (UserLogin) this.iUserLoginService.find(new UserLogin(),
				"where gsno = '" + userLoginBean.getGsno() + "'");
		if (this.message.trim().equals("")) {
			updatedDetails.setId(0);
			this.message = " No data updated ! ";
			return new ResponseEntity(new ErrorResponse(HttpStatus.OK, "0", this.message), HttpStatus.OK);
		}
		this.message += " uploaded successfully ! ";
		logger.info(this.message + " \n " + updatedDetails);
		return new ResponseEntity(updatedDetails, HttpStatus.OK);
	}

	@PutMapping({ "/querydata/forgot/{beanName}" })
	public ResponseEntity<?> forgotPassword(UserLogin userLogin, @PathVariable("beanName") String beanName,
			@RequestParam(value = "forgotQueryId", required = true) String forgotQueryId,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "userid", required = true) String userid,
			@RequestParam(value = "dob", required = true) String dob,
			@RequestParam(value = "password", required = true) String password) {
		logger.info("Requested Bean :" + beanName + ", Requested Parameters : {} , {} ", forgotQueryId, title);
		if (beanName.equalsIgnoreCase("api")) {
			AppQuery saveUserQuery = getAppQuery(forgotQueryId, title);
			if (saveUserQuery == null) {
				String message = "No query found against id : " + forgotQueryId;
				logger.info(message);
				List<ErrorResponse> errorResponseList = new ArrayList();
				errorResponseList.add(new ErrorResponse(HttpStatus.NO_CONTENT, "0", message));
				return new ResponseEntity(errorResponseList, HttpStatus.NO_CONTENT);
			}
			int i = forgotPassword(saveUserQuery, userid, dob, password);
			if (i > 0) {
				this.status.put("id", "" + i);
			}
			return new ResponseEntity(this.status, HttpStatus.OK);
		}
		String message = "Invalid fetch bean name : " + beanName;
		logger.info(message);
		List<ErrorResponse> errorResponseList = new ArrayList();
		errorResponseList.add(new ErrorResponse(HttpStatus.NO_CONTENT, "0", message));
		return new ResponseEntity(errorResponseList, HttpStatus.NO_CONTENT);
	}

	private AppQuery getAppQuery(String id, String title) {
		AppQuery objAppQuery = null;
		if ((id != null) && (title != null)) {
			objAppQuery = (AppQuery) this.iAppQueryService.find(new AppQuery(),
					" where id = " + id + " and title = '" + title + "'");
		} else if (id != null) {
			objAppQuery = (AppQuery) this.iAppQueryService.find(new AppQuery(), " where id = " + id);
		} else if (title != null) {
			objAppQuery = (AppQuery) this.iAppQueryService.find(new AppQuery(), " where title = '" + title + "'");
		}
		return objAppQuery;
	}

	// ,String imei
	private List<ApiBean> checkLogin(AppQuery objAppQuery, String userid, String password, String imei) {
		List<ApiBean> list = null;
		String query = null;
		logger.info("user id   " + userid + " \n   imei  " + imei + "password    " + password);
		// && (imei !=null)
		if ((userid != null) && (password != null) && (imei != null)) {
			logger.info("not any value is null ");
			query = objAppQuery.getQuery().replace("<userid>", userid);
			query = query.replace("<imei>", imei);
			query = query.replace("<password>", password);
		} else {
			logger.info("some value are null ");
			query = objAppQuery.getQuery();
		}
		logger.info("created query  : " + query);
		list = this.iDataGenericService.nativeQuery(new ApiBean(), query);
		logger.info("API List Size : " + list.size());
		return list;
	}

	private List<ApiBean> checkUser(AppQuery objAppQuery, String gsno, String dob) {
		List<ApiBean> list = null;
		String query = null;
		if ((gsno != null) && (dob != null)) {
			query = objAppQuery.getQuery().replace("<gsno>", gsno);
			query = query.replace("<dob>", dob);
		} else {
			query = objAppQuery.getQuery();
		}
		logger.info("query : " + query);
		list = this.iDataGenericService.nativeQuery(new ApiBean(), query);
		logger.info("API List Size : " + list.size());
		return list;
	}

	// ,String imei
	private String userSignup(AppQuery objAppQuery, String gsno, String userid, String dob, String email, String mobile,
			String aadhar, String imei, String password) {
		String query = null;
		query = objAppQuery.getQuery().replace("<gsno>", gsno);

		query = query.replace("<userid>", userid);
		query = query.replace("<dob>", dob);
		if (email == null) {
			query = query.replace("<email>", "");
		} else {
			query = query.replace("<email>", email);
		}
		if (mobile == null) {
			query = query.replace("<mobile>", "");
		} else {
			query = query.replace("<mobile>", mobile);
		}
		if (aadhar == null) {
			query = query.replace("<aadhar>", "");
		} else {
			query = query.replace("<aadhar>", aadhar);
		}
		if (imei == null) {
			query = query.replace("<imei>", "");
		} else {
			query = query.replace("<imei>", imei);
		}

		query = query.replace("<password>", password);

		logger.info("query : " + query);
		String[] querylist = query.split(";");
		logger.info("fist ." + querylist[0]);
		logger.info("second" + querylist[1]);
		int i = this.iDataGenericService.executeQuery(querylist[0]);
		if (i > 0) {
			query = querylist[1];
		}
		return query;
	}

	private int saveGrievance(AppQuery objAppQuery, String gsno, String grievance_type, String grievance_message) {
		String query = null;
		query = objAppQuery.getQuery().replace("<gsno>", gsno);
		query = query.replace("<type>", grievance_type);
		query = query.replace("<message>", grievance_message);
		logger.info("query : " + query);
		int i = this.iDataGenericService.executeQuery(query);
		return i;
	}

	private int userProfileImgUpdate(String value, String gsno, int index) {
		if ((index == 1) && (value != null)) {
			String query = "update user_login set  url = '" + value + "' where gsno = '" + gsno + "'";
			logger.info("query : " + query);
			int i = this.iDataGenericService.executeQuery(query);
			return i;
		}
		if ((index == 2) && (value != null)) {
			String query = "update user_login set  jointphoto_url = '" + value + "' where gsno = '" + gsno + "'";
			logger.info("query : " + query);
			int i = this.iDataGenericService.executeQuery(query);
			return i;
		}
		if ((index == 3) && (value != null)) {
			String query = "update user_login set  email = '" + value + "' where gsno = '" + gsno + "'";
			logger.info("query : " + query);
			int i = this.iDataGenericService.executeQuery(query);
			return i;
		}
		if ((index == 4) && (value != null)) {
			String query = "update user_login set  mobile = '" + value + "' where gsno = '" + gsno + "'";
			logger.info("query : " + query);
			int i = this.iDataGenericService.executeQuery(query);
			return i;
		}
		return 0;
	}

	private int userSignupUpdate(String gsno, String email, String mobile, String aadhar, String password, String url) {
		String query = null;
		if (email != null) {
			query = " email = '" + email + "' ";
		}
		if ((mobile != null) && (email == null)) {
			query = " mobile = '" + mobile + "' ";
		} else if ((mobile != null) && (email != null)) {
			query = query + " , mobile = '" + mobile + "' ";
		}
		if ((aadhar != null) && (mobile == null) && (email == null)) {
			query = " aadhar = '" + aadhar + "' ";
		} else if (aadhar != null) {
			query = query + ", aadhar = '" + aadhar + "' ";
		}
		if ((password != null) && (aadhar == null) && (mobile == null) && (email == null)) {
			query = "password = aes_encrypt('" + password + "','777277')";
		} else if (password != null) {
			query = query + ",password = aes_encrypt('" + password + "','777277')";
		}
		if ((url != null) && (password == null) && (aadhar == null) && (mobile == null) && (email == null)) {
			query = " url = '" + url + "'";
		} else if (url != null) {
			query = query + ", url = '" + url + "'";
		}
		if (query != null) {
			query = "update user_login set " + query + " where gsno = '" + gsno + "'";
		}
		logger.info("query : " + query);
		int i = this.iDataGenericService.executeQuery(query);
		return i;
	}

	private int forgotPassword(AppQuery objAppQuery, String userid, String dob, String password) {
		String query = null;
		query = objAppQuery.getQuery().replace("<userid>", userid);
		query = query.replace("<dob>", dob);
		query = query.replace("<password>", password);

		logger.info("query : " + query);
		int i = this.iDataGenericService.executeQuery(query);
		return i;
	}

	private String uploadDocuments(String gsno, MultipartFile multipartFile, String fileNamePrefix) throws Exception {
		String newDocName = "";
		String newPath = "";
		try {
			if ((gsno != null) || (!multipartFile.isEmpty())) {
				File doc = new File(this.UPLOADED_PROFILE + "\\" + gsno);
				if (!doc.exists()) {
					logger.info(" Dir " + gsno + " NOT Exists");
					doc.mkdirs();
				}
				String fileName = "";

				String s = multipartFile.getOriginalFilename();
				String[] words = s.split("\\s+");
				for (int i = 0; i < words.length; i++) {
					fileName = fileName + words[i].replaceAll(" ", "");
				}
				String folder_name = this.UPLOADED_PROFILE + "/" + gsno + "/" + fileName;

				File file = new File(folder_name);
				if (fileNamePrefix != null) {
					newDocName = fileNamePrefix + "_" + fileName;
				} else {
					newDocName = fileName;
				}
				newPath = this.UPLOADED_PROFILE + "/" + gsno + "/" + newDocName;

				File newFile = new File(newPath);
				if (!file.isDirectory()) {
					file.renameTo(newFile);
				}
				byte[] bytes = multipartFile.getBytes();
				Path path = Paths.get(newPath, new String[0]);
				logger.info("path" + path);
				Files.write(path, bytes, new OpenOption[0]);
			}
		} catch (IOException e) {
			logger.info("IOException : " + e.getMessage());
		}
		return newDocName;
	}

	private String uploadBase64Documents(String uploadPath, String gsno, String base64, String fileNamePrefix)
			throws Exception {
		String newDocName = "";
		String newPath = "";
		try {
			if (((gsno != null) || (!gsno.isEmpty())) && ((base64 != null) || (!base64.isEmpty()))) {
				File doc = new File(uploadPath + "\\" + gsno);
				if (!doc.exists()) {
					logger.info(" Dir " + gsno + " NOT Exists");
					doc.mkdirs();
				}
				newDocName = fileNamePrefix + gsno + ".jpg";
				newPath = uploadPath + "/" + gsno + "/" + newDocName;
				logger.info("rename file : " + newPath);
				Base64 decoder = new Base64();
				byte[] imgBytes = decoder.decode(base64);
				Path path = Paths.get(newPath, new String[0]);
				logger.info("path" + path);
				Files.write(path, imgBytes, new OpenOption[0]);
			}
		} catch (IOException e) {
			logger.info("IOException : " + e.getMessage());
		}
		return newDocName;
	}

}
