package com.example.controller;

import com.example.entity.AppQuery;
import com.example.service.IGenericService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping({"/appquery"})
public class QueryController
{
  private static final Logger logger = LoggerFactory.getLogger(QueryController.class);
  @Autowired
  private IGenericService<AppQuery> iAppQueryService;
  
  @PostMapping
  public ResponseEntity<?> saveAppQuery(AppQuery objAppQuery, @RequestParam(value="title", required=true) String title, @RequestParam(value="query", required=true) String query)
  {
    objAppQuery.setQuery(query);
    objAppQuery.setTitle(title);
    this.iAppQueryService.save(objAppQuery);
    logger.info("Data saved successfully !");
    return new ResponseEntity(HttpStatus.CREATED);
  }
  
  @PutMapping
  public ResponseEntity<?> updateAppQuery(AppQuery objAppQuery, @RequestParam(value="queryId", required=true) String queryId, @RequestParam(value="title", required=false) String title, @RequestParam(value="query", required=false) String query)
  {
    objAppQuery = (AppQuery)this.iAppQueryService.find(new AppQuery(), "where id = " + queryId);
    if (objAppQuery != null)
    {
      objAppQuery.setTitle(title);
      objAppQuery.setQuery(query);
      this.iAppQueryService.update(objAppQuery);
      logger.info("Data update successfully !");
      return new ResponseEntity(HttpStatus.CREATED);
    }
    logger.info("Data not updated!");
    return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
  }
  
  @DeleteMapping
  public ResponseEntity<?> deleteAppQuery(@RequestParam(value="id", required=true) String id)
  {
    AppQuery objAppQuery = (AppQuery)this.iAppQueryService.find(new AppQuery(), "where id = " + id);
    if (objAppQuery == null) {
      return new ResponseEntity(objAppQuery, HttpStatus.NO_CONTENT);
    }
    this.iAppQueryService.delete(objAppQuery);
    logger.info("Data deleted successfully !");
    return new ResponseEntity(HttpStatus.GONE);
  }
  
  @GetMapping
  public ResponseEntity<?> findAppQuery(@RequestParam(value="id", required=false) String id, @RequestParam(value="title", required=false) String title)
  {
    if ((id == null) && (title == null)) {
      return new ResponseEntity(this.iAppQueryService.fetch(new AppQuery()), HttpStatus.OK);
    }
    AppQuery objAppQuery = null;
    if ((id != null) && (title == null)) {
      objAppQuery = (AppQuery)this.iAppQueryService.find(new AppQuery(), "where id = " + id);
    } else if ((id == null) && (title != null)) {
      objAppQuery = (AppQuery)this.iAppQueryService.find(new AppQuery(), " where title = '" + title + "'");
    } else if ((id != null) && (title != null)) {
      objAppQuery = (AppQuery)this.iAppQueryService.find(new AppQuery(), " where id = " + id + " and title = '" + title + "'");
    }
    if (objAppQuery == null) {
      return new ResponseEntity(objAppQuery, HttpStatus.NO_CONTENT);
    }
    logger.info("Data found! ");
    return new ResponseEntity(objAppQuery, HttpStatus.OK);
  }
}
