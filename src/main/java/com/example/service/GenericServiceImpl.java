package com.example.service;

import com.example.dao.IGenericDao;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GenericServiceImpl<T>
  implements IGenericService<T>
{
  @Autowired
  private IGenericDao<T> iGenericDao;
  
  public void save(T entity)
  {
    this.iGenericDao.save(entity);
  }
  
  public void update(T entity)
  {
    this.iGenericDao.update(entity);
  }
  
  public void delete(T entity)
  {
    this.iGenericDao.delete(entity);
  }
  
  public List<T> fetch(T entity)
  {
    return this.iGenericDao.fetch(entity);
  }
  
  public List<T> fetch(T entity, String condition)
  {
    return this.iGenericDao.fetch(entity, condition);
  }
  
  public T find(T entity, Serializable id)
  {
    return this.iGenericDao.find(entity, id);
  }
  
  public T find(T entity, String condition)
  {
    return this.iGenericDao.find(entity, condition);
  }
  
  public boolean exists(T entity, Serializable id)
  {
    return this.iGenericDao.exists(entity, id);
  }
  
  public boolean exists(T entity, String condition)
  {
    return this.iGenericDao.exists(entity, condition);
  }
  
  public List<T> nativeQuery(T entity, String query)
  {
    return this.iGenericDao.nativeQuery(entity, query);
  }
  
  public int executeQuery(String query)
  {
    return this.iGenericDao.executeQuery(query);
  }
}
