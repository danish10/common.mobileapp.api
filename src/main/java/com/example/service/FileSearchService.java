package com.example.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service("documents")
public class FileSearchService
{
  public String searchFile(File file, String gsno, String year, String month)
  {
    String result = "";
    if(month.length()<2) {
    	month="0"+month;
    }
    
    System.out.println(gsno+"  "+year+"  "+month+"  "+file);
    if (file.isDirectory())
    {
      System.out.println("Searching directory ... " + file.getAbsoluteFile());
      if (file.canRead()) {
        for (File temp : file.listFiles()) {
          if ((!temp.isDirectory()) && 
//            (temp.getName().toLowerCase().contains(gsno.toLowerCase())) && (temp.getName().toLowerCase().contains("_" + year.toLowerCase())) && (temp.getName().toLowerCase().contains("_" + month + "_".toLowerCase()))) {
//            result = temp.getAbsoluteFile().getName();
        		  (temp.getName().contains("GS"+gsno+"H_"+month+"_"+year.toLowerCase()))) {
              result = temp.getAbsoluteFile().getName();
          }
        }
      } else {
        System.out.println(file.getAbsoluteFile() + "Permission Denied");
      }
    }
    return result;
  }
  
  public String searchForm16(File file,String gsno,String year)
  {
    String result = "";
   System.out.println("file "+file+" "+ gsno+" "+year);
   
    if (file.isDirectory())
    {
      System.out.println("Searching directory ... " + file.getAbsoluteFile());
      if (file.canRead()) {
        for (File temp : file.listFiles()) {
          if ((!temp.isDirectory()) && 
            (temp.getName().contains("GS"+gsno+"H_"+year.toLowerCase()))) {
            result = temp.getAbsoluteFile().getName();
          }
        }
      } else {
        System.out.println(file.getAbsoluteFile() + "Permission Denied");
      }
    }
    else {
    	System.out.println("else  search form 16"+result);
    }
    System.out.println(" search form 16"+result);
  
    return result;
  }
  
  public List<String> searchAllFiles(File file)
  {
    List<String> result = new ArrayList();
    if (file.isDirectory())
    {
      System.out.println("Searching directory ... " + file.getAbsoluteFile());
      if (file.canRead()) {
        for (File temp : file.listFiles()) {
          if (temp.isDirectory()) {
            searchAllFiles(temp);
          } else {
            result.add(temp.getAbsoluteFile().getName());
          }
        }
      } else {
        System.out.println(file.getAbsoluteFile() + "Permission Denied");
      }
    }
    return result;
  }
  
  public List<String> searchAllPathFiles(File file)
  {
    List<String> result = new ArrayList();
    if (file.isDirectory())
    {
      System.out.println("Searching directory ... " + file.getAbsoluteFile());
      if (file.canRead()) {
        for (File temp : file.listFiles()) {
          if (!temp.isDirectory()) {
            result.add(temp.getAbsoluteFile().getName());
          }
        }
      } else {
        System.out.println(file.getAbsoluteFile() + "Permission Denied");
      }
    }else {
    	System.out.println("else searching path");
    }
    return result;
  }
}
