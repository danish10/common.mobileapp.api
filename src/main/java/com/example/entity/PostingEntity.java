package com.example.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="postings")
public class PostingEntity implements Serializable{

	private static final long serialVersionUID = -5049840060934544077L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;                  
	private String gsno;                
	private String from_unit;           
	private String to_unit;                    
	private String posting_order_no;            
	private String nrs;                         
	private String movedate;
	private String insertDate;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getGsno() {
		return gsno;
	}
	public void setGsno(String gsno) {
		this.gsno = gsno;
	}
	public String getFrom_unit() {
		return from_unit;
	}
	public void setFrom_unit(String from_unit) {
		this.from_unit = from_unit;
	}
	public String getTo_unit() {
		return to_unit;
	}
	public void setTo_unit(String to_unit) {
		this.to_unit = to_unit;
	}
	public String getPosting_order_no() {
		return posting_order_no;
	}
	public void setPosting_order_no(String posting_order_no) {
		this.posting_order_no = posting_order_no;
	}
	public String getNrs() {
		return nrs;
	}
	public void setNrs(String nrs) {
		this.nrs = nrs;
	}
	public String getMovedate() {
		return movedate;
	}
	public void setMovedate(String movedate) {
		this.movedate = movedate;
	}
	
	public String getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(String insertDate) {
		this.insertDate = insertDate;
	}
	@Override
	public String toString() {
		return "PostingEntity [id=" + id + ", gsno=" + gsno + ", from_unit=" + from_unit + ", to_unit=" + to_unit
				+ ", posting_order_no=" + posting_order_no + ", nrs=" + nrs + ", movedate=" + movedate + ", insertDate="
				+ insertDate + "]";
	}
	
}
