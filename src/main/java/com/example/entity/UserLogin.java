package com.example.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="user_login")
public class UserLogin {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id") 
	private long id;
	@Column(name = "userid",unique=true,length=100,nullable=false) 
	private String userid;
	@Column(name = "gsno",unique=true,length=12,nullable=false) 
	private String gsno;
	@Column(name = "dob",nullable=false)
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date dob;	
	@Column(name = "aadhar",unique=true,length=12,nullable=true) 
	private String aadhar;
	@Column(name = "email",unique=true,length=100,nullable=true) 
	private String email;
	@Column(name = "mobile",unique=true,length=12,nullable=true) 
	private String mobile;
	@Column(name = "password",length=500,nullable=false) 
	private String password;
	@Column(name="imei", nullable=false)
	private String imei;
	private String url;
	@Column(name="jointphoto_url")
	private String jointPhoto_url;
	private String wifePhoto_url;
	
	
	public UserLogin() {
		super();
		// TODO Auto-generated constructor stub
	}


	public UserLogin(long id, String userid, String gsno, Date dob, String aadhar, String email, String mobile,
			String password, String imei, String url, String jointPhoto_url, String wifePhoto_url) {
		super();
		this.id = id;
		this.userid = userid;
		this.gsno = gsno;
		this.dob = dob;
		this.aadhar = aadhar;
		this.email = email;
		this.mobile = mobile;
		this.password = password;
		this.imei = imei;
		this.url = url;
		this.jointPhoto_url = jointPhoto_url;
		this.wifePhoto_url = wifePhoto_url;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getUserid() {
		return userid;
	}


	public void setUserid(String userid) {
		this.userid = userid;
	}


	public String getGsno() {
		return gsno;
	}


	public void setGsno(String gsno) {
		this.gsno = gsno;
	}


	public Date getDob() {
		return dob;
	}


	public void setDob(Date dob) {
		this.dob = dob;
	}


	public String getAadhar() {
		return aadhar;
	}


	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getImei() {
		return imei;
	}


	public void setImei(String imei) {
		this.imei = imei;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public String getJointPhoto_url() {
		return jointPhoto_url;
	}


	public void setJointPhoto_url(String jointPhoto_url) {
		this.jointPhoto_url = jointPhoto_url;
	}


	public String getWifePhoto_url() {
		return wifePhoto_url;
	}


	public void setWifePhoto_url(String wifePhoto_url) {
		this.wifePhoto_url = wifePhoto_url;
	}


	@Override
	public String toString() {
		return "UserLogin [id=" + id + ", userid=" + userid + ", gsno=" + gsno + ", dob=" + dob + ", aadhar=" + aadhar
				+ ", email=" + email + ", mobile=" + mobile + ", password=" + password + ", imei=" + imei + ", url="
				+ url + ", jointPhoto_url=" + jointPhoto_url + ", wifePhoto_url=" + wifePhoto_url + "]";
	}
	
	
	
	
	 
}
