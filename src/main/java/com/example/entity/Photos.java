package com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="photos")
public class Photos
{
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name="id")
  private long id;
  private String gsno;
  private String photo_self;
  private String photo_wife;
  private String photo_joint;
  private String image_url;
  
  public Photos() {}
  
  public Photos(long id, String gsno, String photo_self, String photo_wife, String photo_joint, String image_url)
  {
    this.id = id;
    this.gsno = gsno;
    this.photo_self = photo_self;
    this.photo_wife = photo_wife;
    this.photo_joint = photo_joint;
    this.image_url = image_url;
  }
  
  public long getId()
  {
    return this.id;
  }
  
  public void setId(long id)
  {
    this.id = id;
  }
  
  public String getGsno()
  {
    return this.gsno;
  }
  
  public void setGsno(String gsno)
  {
    this.gsno = gsno;
  }
  
  public String getPhoto_self()
  {
    return this.photo_self;
  }
  
  public void setPhoto_self(String photo_self)
  {
    this.photo_self = photo_self;
  }
  
  public String getPhoto_wife()
  {
    return this.photo_wife;
  }
  
  public void setPhoto_wife(String photo_wife)
  {
    this.photo_wife = photo_wife;
  }
  
  public String getPhoto_joint()
  {
    return this.photo_joint;
  }
  
  public void setPhoto_joint(String photo_joint)
  {
    this.photo_joint = photo_joint;
  }
  
  public String getImage_url()
  {
    return this.image_url;
  }
  
  public void setImage_url(String image_url)
  {
    this.image_url = image_url;
  }
  
  public String toString()
  {
    return "Photos [id=" + this.id + ", gsno=" + this.gsno + ", photo_self=" + this.photo_self + ", photo_wife=" + this.photo_wife + ", photo_joint=" + this.photo_joint + ", image_url=" + this.image_url + "]";
  }
}
