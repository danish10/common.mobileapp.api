package com.example.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_account")
public class UserEntity implements Serializable {
	
	private static final long serialVersionUID = -42675707433339754L;

	@Id
	@Column(name = "loginId")
	private String loginId;

	@Column(name = "userName")
	private String userName;

	@Column(name="user_type")
	private String userType;
	
	@Column(name = "password")
	private String password;

	@Column(name = "mobile")
	private String mobile;

	@Column(name = "emailId")
	private String emailId;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "createDate")
	private String createDate;

   @Column(name="flag")
   private boolean flag;
	
	
	// @Column(name="value",columnDefinition = "boolean default false")

	@Column(name = "appReport", columnDefinition = "boolean default false")
	private boolean appReportView;

	@Column(name = "blacklistReport", columnDefinition = "boolean default false")
	private boolean blacklistReportView;

	@Column(name = "documentuploadReport", columnDefinition = "boolean default false")
	private boolean documentuploadReportView;

	
	public UserEntity() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getLoginId() {
		return loginId;
	}


	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getUserType() {
		return userType;
	}


	public void setUserType(String userType) {
		this.userType = userType;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public String getCreateDate() {
		return createDate;
	}


	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}


	public boolean isFlag() {
		return flag;
	}


	public void setFlag(boolean flag) {
		this.flag = flag;
	}


	public boolean isAppReportView() {
		return appReportView;
	}


	public void setAppReportView(boolean appReportView) {
		this.appReportView = appReportView;
	}


	public boolean isBlacklistReportView() {
		return blacklistReportView;
	}


	public void setBlacklistReportView(boolean blacklistReportView) {
		this.blacklistReportView = blacklistReportView;
	}


	public boolean isDocumentuploadReportView() {
		return documentuploadReportView;
	}


	public void setDocumentuploadReportView(boolean documentuploadReportView) {
		this.documentuploadReportView = documentuploadReportView;
	}


	@Override
	public String toString() {
		return "UserEntity [loginId=" + loginId + ", userName=" + userName + ", userType=" + userType + ", password="
				+ password + ", mobile=" + mobile + ", emailId=" + emailId + ", createdBy=" + createdBy
				+ ", createDate=" + createDate + ", flag=" + flag + ", appReportView=" + appReportView
				+ ", blacklistReportView=" + blacklistReportView + ", documentuploadReportView="
				+ documentuploadReportView + "]";
	}


	
}

	