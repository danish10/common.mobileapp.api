package com.example.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="user_login_details")
public class UserLoginDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5308266942965693381L;


	public UserLoginDetails() {
		// TODO Auto-generated constructor stub
	}

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id") 
	private long id;
	
	@Column(name = "gsno",unique=true,length=12,nullable=false) 
	private String gsno;
	
	@Column(name = "name") 
	private String name;
	
	@Column(name = "dob",nullable=false)
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date dob;
	
	@Column(name = "aadhar",unique=true,length=12,nullable=true) 
	private String aadhar;
	
	@Column(name = "mobile",unique=true,length=12,nullable=true) 
	private String mobile;
	
	
	@Column(name = "download_apk") 
	private String download_apk;

	@Column(name="date_time")
	private String date_time; 
	
	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getGsno() {
		return gsno;
	}


	public void setGsno(String gsno) {
		this.gsno = gsno;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Date getDob() {
		return dob;
	}


	public void setDob(Date dob) {
		this.dob = dob;
	}


	public String getAadhar() {
		return aadhar;
	}


	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getDownload_apk() {
		return download_apk;
	}


	public void setDownload_apk(String download_apk) {
		this.download_apk = download_apk;
	}


	public String getDate_time() {
		return date_time;
	}


	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}


	public UserLoginDetails(long id, String gsno, String name, Date dob, String aadhar, String mobile,
			String download_apk, String date_time) {
		super();
		this.id = id;
		this.gsno = gsno;
		this.name = name;
		this.dob = dob;
		this.aadhar = aadhar;
		this.mobile = mobile;
		this.download_apk = download_apk;
		this.date_time = date_time;
	}


	@Override
	public String toString() {
		return "UserLoginDetails [id=" + id + ", gsno=" + gsno + ", name=" + name + ", dob=" + dob + ", aadhar="
				+ aadhar + ", mobile=" + mobile + ", download_apk=" + download_apk + ", date_time=" + date_time + "]";
	}


 


	
}
