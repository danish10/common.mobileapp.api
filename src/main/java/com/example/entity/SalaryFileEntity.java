package com.example.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_salaryfile")
public class SalaryFileEntity implements Serializable {
	
	private static final long serialVersionUID = 6870067405078737048L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private long id;
	
	@Column(name="CreatedBy")
	private String loginId;
	
	@Column(name="gsno")
	private String gsno;
	
	@Column(name="date")
	private String date;
	
	@Column(name="salaryPDFFile")
	private String salaryPDFFile;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getSalaryPDFFile() {
		return salaryPDFFile;
	}

	public void setSalaryPDFFile(String salaryPDFFile) {
		this.salaryPDFFile = salaryPDFFile;
	}
	
	

	public String getGsno() {
		return gsno;
	}

	public void setGsno(String gsno) {
		this.gsno = gsno;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "SalaryFileEntity [id=" + id + ", loginId=" + loginId + ", gsno=" + gsno + ", date=" + date
				+ ", salaryPDFFile=" + salaryPDFFile + "]";
	}

	
}
