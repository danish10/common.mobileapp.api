package com.example.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="blacklist_user")
public class BlackListEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1932049397081570568L;


	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id") 
	private long id;
	
	@Column(name = "gsno",unique=true,length=12,nullable=false) 
	private String gsno;
	
	@Column(name = "name") 
	private String name;
	
	@Column(name = "mobile",unique=true,length=12,nullable=true) 
	private String mobile;

	@Column(name = "dob",unique=true,length=12,nullable=true) 
	private String dob;
	
	@Column(name = "datetime") 
	private String datetime;

	@Column(name = "blackListBy") 
	private String blackListBy;
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getGsno() {
		return gsno;
	}

	public void setGsno(String gsno) {
		this.gsno = gsno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getBlackListBy() {
		return blackListBy;
	}

	public void setBlackListBy(String blackListBy) {
		this.blackListBy = blackListBy;
	}

	@Override
	public String toString() {
		return "BlackListEntity [id=" + id + ", gsno=" + gsno + ", name=" + name + ", mobile=" + mobile + ", dob=" + dob
				+ ", datetime=" + datetime + ", blackListBy=" + blackListBy + "]";
	}

	
}
