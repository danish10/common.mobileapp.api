package com.example.dao;

import java.io.Serializable;
import java.util.List;

public abstract interface IGenericDao<T>
{
  public abstract void save(T paramT);
  
  public abstract void update(T paramT);
  
  public abstract void delete(T paramT);
  
  public abstract List<T> fetch(T paramT);
  
  public abstract List<T> fetch(T paramT, String paramString);
  
  public abstract T find(T paramT, Serializable paramSerializable);
  
  public abstract T find(T paramT, String paramString);
  
  public abstract boolean exists(T paramT, Serializable paramSerializable);
  
  public abstract boolean exists(T paramT, String paramString);
  
  public abstract List<T> nativeQuery(T paramT, String paramString);
  
  public abstract int executeQuery(String paramString);

  
}
